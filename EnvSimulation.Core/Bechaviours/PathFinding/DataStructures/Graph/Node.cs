﻿using EnvSimulation.Core.Envirnoment;
using System.Collections.Generic;

namespace EnvSimulation.Core.Behaviours.PathFinding.DataStructures.Graph
{
    public class Node
    {
        public Position position;
        public List<Edge> edges;
        public bool visited;

        public Node(Position position)
        {
            this.position = position;
            edges = new List<Edge>();
            visited = false;
        }
        
    }
}

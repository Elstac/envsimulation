﻿using EnvSimulation.Core.Behaviours.PathFinding.DataStructures.Graph;
using EnvSimulation.Core.Behaviours.PathFinding.DataStructures.Heap;
using EnvSimulation.Core.Envirnoment;

namespace EnvSimulation.Core.Behaviours.PathFinding
{
    class PathFinder
    {
        private MinHeap queue;
        private int[,] d;
        private Position[,] v;
        protected Graph graph;

        private Position source;
        private Position target;

        public PathFinder(Terrain terrain, Position source, Position target)
        {
            var size = terrain.GetSize();

            this.target = target;
            this.source = source;

            InitializeGraph(terrain);

            d = new int[size.Width, size.Height];
            v = new Position[size.Width, size.Height];
            for (int i = 0; i < size.Width; i++)
                for (int j = 0; j < size.Height; j++)
                {
                    d[i, j] = -1;
                    v[i, j] = new Position { X = -1, Y = -1 };
                }

            queue = new MinHeap(size.Width * size.Height);
        }

        private void InitializeGraph(Terrain terrain)
        {
            var size = terrain.GetSize();

            graph = new Graph(terrain);
            if(!terrain.GetField(source.X,source.Y).IsPassable())
                graph.AddNode(source);

            if (!terrain.GetField(target.X, target.Y).IsPassable())
                graph.AddNode(target);


            for (int x = 0; x < size.Width; x++)
            {
                for (int y = 0; y < size.Height; y++)
                {
                    var field = terrain.GetField(x, y);

                    if (!field.IsPassable()&&!field.Position.Equals(source) && !field.Position.Equals(target))
                        continue;

                    var adjFields = terrain.GetAdjecentFields(x, y);

                    foreach (var adjField in adjFields)
                    {
                        if (!adjField.IsPassable() && !adjField.Position.Equals(source) && !adjField.Position.Equals(target))
                            continue;

                        if (graph.EdgeExists(field.Position, adjField.Position))
                            continue;

                        graph.AddEdge(field.Position, adjField.Position, 1);
                    }
                }
            }
        }

        public void Run()
        {
            Position num;
            var pointer = graph.GetNode(source);

            while(pointer != null)
            {
                num = pointer.position;
                if (pointer.visited)
                {
                    pointer = graph.GetNode(queue.Pop());
                    continue;
                }
                
                foreach (var e in pointer.edges)
                {
                    if (e.target.visited)
                        continue;

                    if (d[e.target.position.X, e.target.position.Y] == -1|| d[num.X,num.Y] + e.cost < d[e.target.position.X, e.target.position.Y])
                    {
                        d[e.target.position.X, e.target.position.Y] = d[num.X,num.Y] + e.cost;
                        v[e.target.position.X, e.target.position.Y] = pointer.position;

                        queue.Add(d[e.target.position.X, e.target.position.Y], e.target.position);
                    }
                }

                pointer.visited = true;
                pointer = graph.GetNode(queue.Pop());
            }

        }

        public Move GetClosestMove()
        {
            Position lastPosition= new Position();
            var position = target;

            while(!position.Equals(source)&& position.X != -1 && position.Y != -1)
            {
                lastPosition = new Position { X=position.X, Y = position.Y};
                position = v[position.X, position.Y];
            }

            return new Move { X = lastPosition.X - source.X, Y = lastPosition.Y - source.Y };
        }

    }
}

﻿using EnvSimulation.Core.Creatures;
using EnvSimulation.Core.Envirnoment;
using EnvSimulation.Core.Senses;
using EnvSimulation.Shared.RNG;
using System;
using System.Collections.Generic;

namespace EnvSimulation.Core.Behaviours
{
    public class RandomBehaviour : ICreatureBehaviour
    {
        private IRandomGenerator generator;

        public RandomBehaviour(IRandomGenerator generator)
        {
            this.generator = generator;
        }

        public Move GetNextMove(CreatureState state, Species species, CreatureVision vision)
        {
            var creaturePosition = vision.GetCreaturePosition();

            var avaliablePositions = new List<Move>
            {
                new Move{X=1,Y=0},
                new Move{X=-1,Y=0},
                new Move{X=0,Y=1},
                new Move{X=0,Y=-1}
            };

            while (avaliablePositions.Count > 0)
            {
                var randomIndex = generator.GetRandomInt(0, avaliablePositions.Count-1);
                var move = avaliablePositions[randomIndex];
                avaliablePositions.Remove(move);
                var nextField = vision.TerrainFields[creaturePosition.X + move.X, creaturePosition.Y + move.Y];

                if (nextField.IsPassable())
                    return move;
            }

            return new Move { X = 0, Y = 0};
        }
    }
}

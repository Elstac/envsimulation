﻿namespace EnvSimulation.Core.Communication
{
    public class FieldState
    {
        public int TerrainType { get; set; }
        public CreatureState Creature { get; set; }
        public ElementState[] Elements { get; set; }
    }
}

﻿namespace EnvSimulation.Core.Communication
{
    public class WorldState
    {
        public FieldState[,] Fields { get; set; }
    }
}

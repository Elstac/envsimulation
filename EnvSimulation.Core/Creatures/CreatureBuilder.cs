﻿using EnvSimulation.Core.Behaviours;
using EnvSimulation.Core.Envirnoment;
using EnvSimulation.Core.Senses;
using EnvSimulation.Shared.Helpers;
using System;

namespace EnvSimulation.Core.Creatures
{
    public class CreatureBuilder
    {
        private ISense sense;
        private ICreatureBehaviour creatureBehaviour;
        private Position position;
        private Species species; 

        public CreatureBuilder()
        {
            sense = new NullSense();
            creatureBehaviour = new NullBehaviour();
            position = new Position { X = 0, Y = 0 };

            var speciesName = RandomStringGenerator.GetRandomString(5);
            species = new Species(speciesName);
        }

        public CreatureBuilder WithSense(ISense sense)
        {
            this.sense = sense;
            return this;
        }

        public CreatureBuilder WithPosition(int x, int y)
        {
            position = new Position { X = x, Y = y };
            return this;
        }

        public CreatureBuilder WithBehaviour(ICreatureBehaviour creatureBehaviour)
        {
            this.creatureBehaviour = creatureBehaviour;
            return this;
        }

        public CreatureBuilder OfSpecies(string speciesName)
        {
            species = new Species(speciesName);
            return this;
        }

        public Creature Build()
        {
            return new Creature(creatureBehaviour, sense, position,species);
        }
    }
}

﻿using System;

namespace EnvSimulation.Core.Creatures
{
    public class CreatureState
    {
        private int hunger;
        private int health;

        public delegate void NegativeStateEventHandler();
        public event NegativeStateEventHandler NegativeStateEvent;

        public CreatureState()
        {
            hunger = 100;
            health = 10;
        }

        internal void Feed(int v)
        {
            hunger += v;
        }

        public CreatureState(int hunger)
        {
            this.hunger = hunger;
        }

        public int Hunger { get => hunger; }
        public int Health { get => health; }

        public void DealDamage(int damage)
        {
            health -= damage;

            if (health <= 0)
                NegativeStateEvent();
        }

        public void NextStep()
        {
            hunger -= 3;

            if(hunger == 0)
                NegativeStateEvent();
        }
    }
}

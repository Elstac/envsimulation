﻿using EnvSimulation.Core.Creatures;
using System;

namespace EnvSimulation.Core.Envirnoment.Elements
{
    public class Food : IInteractor
    {
        public event IInteractor.OnDestroyEventHandler OnDestroy;

        public void Interact(Creature interactor)
        {
            interactor.State.Feed(10);
            OnDestroy(this);
        }
    }
}

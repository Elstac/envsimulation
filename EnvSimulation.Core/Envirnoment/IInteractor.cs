﻿using EnvSimulation.Core.Creatures;

namespace EnvSimulation.Core.Envirnoment
{
    public interface IInteractor
    {
        void Interact(Creature interactor);

        delegate void OnDestroyEventHandler(IInteractor sender);
        event OnDestroyEventHandler OnDestroy;
    }
}

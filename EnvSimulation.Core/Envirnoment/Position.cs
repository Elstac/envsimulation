﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace EnvSimulation.Core.Envirnoment
{
    public class Position:IEquatable<Position>
    {
        public int X { get; set; }
        public int Y { get; set; }

        public static Position GetRendomPosition(int x, int y)
        {
            var rng = new Random();

            return new Position { X = rng.Next(0, x + 1), Y = rng.Next(0, y + 1) };
        }

        public bool Equals([AllowNull] Position other)
        {
            return X == other.X && Y == other.Y;
        }

    }
}

﻿using System.Collections.Generic;
using System.Drawing;

namespace EnvSimulation.Core.Envirnoment
{
    public class Terrain
    {
        private TerrainField[,] fields;

        public Terrain(TerrainField[,] fields)
        {
            this.fields = fields;
            var size = GetSize();
            for (int x = 0; x < size.Width; x++)
            {
                for (int y = 0; y < size.Height; y++)
                {
                    fields[x, y].Position = new Position { X = x, Y = y };
                }
            }
        }

        public Size GetSize()
        {
            var width = fields.GetLength(0);
            var height = fields.GetLength(1);

            return new Size(width, height);
        }

        public TerrainField GetField(int x, int y)
        {
            return fields[x, y];
        }

        public List<TerrainField> GetAdjecentFields(int x, int y)
        {
            var adjcentFields = new List<TerrainField>();
            var size = GetSize();

            if (x - 1 >= 0)
                adjcentFields.Add(fields[x - 1, y]);
            if (y - 1 >= 0)
                adjcentFields.Add(fields[x, y - 1]);
            if (x + 1 < size.Width)
                adjcentFields.Add(fields[x + 1, y]);
            if (y + 1 < size.Height)
                adjcentFields.Add(fields[x, y + 1]);

            return adjcentFields;
        }

        public void AddInteractor(IInteractor interactor, int x, int y)
        {
            fields[x, y].AddInteractor(interactor);
        }

        public void RemoveElement(IInteractor element)
        {
            foreach (var field in fields)
            {
                if (field.RemoveInteractor(element))
                    return;
            }
        }

        private bool IsPositionValid(Position position)
        {
            var size = GetSize();

            return !(position.X < 0 || position.Y < 0 || position.X >= size.Width || position.Y >= size.Height);
        }

        public void RemoveElements(Position elementsPosition)
        {
            if (!IsPositionValid(elementsPosition))
                return;

            var field = fields[elementsPosition.X, elementsPosition.Y];

            field.ClearInteractors();
        }

        public static Terrain GetEmptyTerrain()
        {
            return new Terrain(new TerrainField[,] { });
        }
    }
}

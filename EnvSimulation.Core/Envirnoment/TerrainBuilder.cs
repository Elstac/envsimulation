﻿using System;

namespace EnvSimulation.Core.Envirnoment
{
    public class TerrainBuilder
    {
        private Terrain terrain;
        private IInteractor.OnDestroyEventHandler eventHandler;

        public TerrainBuilder(IInteractor.OnDestroyEventHandler eventHandler)
        {
            this.eventHandler = eventHandler;
        }
         
        public TerrainBuilder()
        {

        }

        public TerrainBuilder BuildRandomTerrain(int size)
        {
            terrain = BuildTerrain(size,size, () =>
            {
                var rng = new Random();
                return rng.Next(0, 2) == 1 ? TerrainValue.Ground : TerrainValue.Obstacle;
            });

            return this;
        }

        public TerrainBuilder BuildEmptyTerrain(int size)
        {
            terrain = BuildTerrain(size, size,()=>TerrainValue.Ground);
            return this;
        }

        public TerrainBuilder BuildEmptyTerrain(int width, int height)
        {
            terrain = BuildTerrain(width, height, () => TerrainValue.Ground);
            return this;
        }

        private Terrain BuildTerrain(int width, int height, Func<TerrainValue> ValueGenerator)
        {
            var terrainArray = new TerrainField[width, height];

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    terrainArray[i, j] = new TerrainField(ValueGenerator());
                }
            }

            return new Terrain(terrainArray);
        }

        public TerrainBuilder WithCustomTileType(TerrainValue terrainValue, int posX, int posY)
        {
            terrain.GetField(posX, posY).Value = terrainValue;

            return this;
        }

        public TerrainBuilder WithCustomInteractor(IInteractor interactor,int posX, int posY)
        {
            terrain.AddInteractor(interactor, posX, posY);

            if (eventHandler != null)
                interactor.OnDestroy += eventHandler;

            return this;
        }

        public Terrain Build()
        {
            return terrain is null ? Terrain.GetEmptyTerrain() : terrain;
        }
    }
}

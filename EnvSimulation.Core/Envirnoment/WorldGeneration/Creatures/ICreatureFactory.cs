﻿using EnvSimulation.Core.Creatures;
using System.Collections.Generic;
using System.Drawing;

namespace EnvSimulation.Core.Envirnoment.WorldGeneration.Creatures
{
    public interface ICreatureFactory
    {
        List<Creature> GetSpeciesGroup(Terrain terrain);
    }
}

﻿using EnvSimulation.Core.Behaviours;
using EnvSimulation.Shared.RNG;

namespace EnvSimulation.Core.Envirnoment.WorldGeneration.Creatures
{
    class PassiveCreatureFactory : CreatureFactory
    {
        public PassiveCreatureFactory(int speciesNumber, int creaturePerSpecies) : base(speciesNumber, creaturePerSpecies)
        {
        }

        protected override ICreatureBehaviour GetBehaviour()
        {
            return new SimpleBehaviour(new RandomBehaviour(new RandomGenerator()));
        }
    }
}

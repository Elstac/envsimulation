﻿namespace EnvSimulation.Core.Envirnoment.WorldGeneration
{
    class EmptyTerrainFactory : TerrainFactory
    {
        private readonly string terrainSize;

        public EmptyTerrainFactory(string terrainSize, string foodDensity) : base(terrainSize, foodDensity)
        {
        }

        protected override Terrain GenerateTerrain(int width, int height)
        {
            return new TerrainBuilder().BuildEmptyTerrain(width, height).Build();
        }
    }
}

﻿namespace EnvSimulation.Core.Envirnoment.WorldGeneration
{
    public interface ITerrainFactory
    {
        Terrain GetTerrain(IInteractor.OnDestroyEventHandler foodHandler);
    }
}

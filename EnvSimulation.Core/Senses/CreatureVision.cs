﻿using EnvSimulation.Core.Envirnoment;

namespace EnvSimulation.Core.Senses
{
    public class CreatureVision
    {
        public CreatureVision(TerrainField[,] terrainFields)
        {
            TerrainFields = terrainFields;
        }

        public TerrainField[,] TerrainFields { get; set; }

        public Position GetCreaturePosition()
        {
            var middle = TerrainFields.GetLength(0) / 2;

            return new Position { X = middle, Y = middle };
        }

        public Terrain AsTerrain()
        {
            return new Terrain(TerrainFields);
        }

        public static CreatureVision CreateEmpty() => new CreatureVision(new TerrainField[0, 0]);
    }
}

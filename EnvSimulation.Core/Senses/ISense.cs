﻿using EnvSimulation.Core.Envirnoment;

namespace EnvSimulation.Core.Senses
{
    public interface ISense
    {
        CreatureVision GetVision(Terrain terrain,Position actualPosition);
    }
}

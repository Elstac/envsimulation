﻿using EnvSimulation.Core.Envirnoment;

namespace EnvSimulation.Core.Senses
{
    class NullSense : ISense
    {
        public CreatureVision GetVision(Terrain terrain, Position actualPosition)
        {
            return new CreatureVision(new TerrainField[,] { });
        }
    }
}

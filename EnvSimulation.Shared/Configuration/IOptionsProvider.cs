﻿using System;
using System.Collections.Generic;

namespace EnvSimulation.Shared.Configuration
{
    public interface IOptionsProvider
    {
        TOptions GetOptions<TOptions>() where TOptions : class;
        void PutOptions<TOptions>(TOptions optionsToAdd) where TOptions : class, IOptionSet;
    }

    public class OptionsProvider : IOptionsProvider
    {
        private Dictionary<Type,IOptionSet> options;

        public OptionsProvider()
        {
            options = new Dictionary<Type, IOptionSet>();
        }

        public TOptions GetOptions<TOptions>() where TOptions:class
        {
            if (!options.ContainsKey(typeof(TOptions)))
                throw new ArgumentException($"No options of type {typeof(TOptions)} provided");

            return options[typeof(TOptions)] as TOptions;
        }

        public void PutOptions<TOptions>(TOptions optionsToAdd) where TOptions : class, IOptionSet
        {
            if (options.ContainsKey(typeof(TOptions)))
                throw new ArgumentException($"Options of type {typeof(TOptions)} provided already");

            options.Add(typeof(TOptions), optionsToAdd);
        }
    }
}

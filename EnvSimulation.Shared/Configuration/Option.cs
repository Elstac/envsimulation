﻿namespace EnvSimulation.Shared.Configuration
{
    public class Option
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}

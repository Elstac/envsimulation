﻿using System;

namespace EnvSimulation.Shared.Helpers
{
    public static class DistanceHelper
    {
        public static bool InRange(int x1, int y1, int x2, int y2, int range)
        {
            return (Math.Pow(x1 - x2, 2) + Math.Pow(y1 - y2, 2)) <= (range * range);
        }
    }
}

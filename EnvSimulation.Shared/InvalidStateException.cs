﻿using System;

namespace EnvSimulation.Shared
{
    public class InvalidStateException : Exception
    {
        public InvalidStateException(string message) : base(message)
        {
        }
    }
}

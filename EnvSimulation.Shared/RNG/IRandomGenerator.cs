﻿namespace EnvSimulation.Shared.RNG
{
    public interface IRandomGenerator
    {
        int GetRandomInt(int min, int max);
    }
}

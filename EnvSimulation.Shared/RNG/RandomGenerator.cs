﻿using System;

namespace EnvSimulation.Shared.RNG
{
    public class RandomGenerator : IRandomGenerator
    {
        public int GetRandomInt(int min, int max)
        {
            return new Random().Next(min, max + 1);
        }
    }
}

﻿using EnvSimulation.Core.Creatures;
using EnvSimulation.Core.Envirnoment;
using EnvSimulation.Core.Senses;
using System;
using System.Collections.Generic;

namespace EnvSimulation.Core.Behaviours
{
    class AggressiveBehaviour : CreatureBehaviour
    {
        public AggressiveBehaviour() : base()
        {
        }

        public AggressiveBehaviour(ICreatureBehaviour next):base(next)
        {
        }

        protected override Position GetTarget(CreatureState state, CreatureVision vision, Species species)
        {
            var terrain = vision.AsTerrain();
            var terrainSize = terrain.GetSize();

            var positionsToCheck = new List<Position>();

            for (int x = 0; x < terrainSize.Width; x++)
            {
                for (int y = 0; y < terrainSize.Height; y++)
                {
                    positionsToCheck.Add(new Position { X = x, Y = y });
                }
            }

            foreach (var position in positionsToCheck)
            {
                var other = vision.TerrainFields[position.X, position.Y].Creature;
                if (other != null && other.SpeciesName != species.Name)
                {
                    return position;
                }
            }

            return null;
        }
    }
}

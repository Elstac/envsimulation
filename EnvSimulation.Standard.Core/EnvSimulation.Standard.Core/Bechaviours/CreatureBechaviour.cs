﻿using EnvSimulation.Core.Behaviours.PathFinding;
using EnvSimulation.Core.Creatures;
using EnvSimulation.Core.Envirnoment;
using EnvSimulation.Core.Senses;
using System;

namespace EnvSimulation.Core.Behaviours
{
    abstract class CreatureBehaviour : ICreatureBehaviour
    {
        private ICreatureBehaviour next;
        public CreatureBehaviour()
        {
            next = new NullBehaviour();
        }

        public CreatureBehaviour(ICreatureBehaviour next)
        {
            this.next = next;
        }
        public Move GetNextMove(CreatureState state, Species species, CreatureVision vision)
        {
            var terrain = vision.TerrainFields;

            if (terrain.Length == 0)
                return new Move { X = 0, Y = 0 };

            var creaturePosition = vision.GetCreaturePosition();
            

            var target = GetTarget(state,vision,species);

            if (!(target is null))
            {
                var pathFinder = new PathFinder(vision.AsTerrain(), creaturePosition, target);
                pathFinder.Run();
                var move = pathFinder.GetClosestMove();
                if (Math.Abs(move.X) <= 1 && Math.Abs(move.Y )<= 1)
                    return move;
            }

            return next.GetNextMove(state, species, vision);
        }

        protected abstract Position GetTarget(CreatureState state, CreatureVision vision, Species species);
    }
}

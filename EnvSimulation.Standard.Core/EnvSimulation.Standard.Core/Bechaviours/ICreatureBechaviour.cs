﻿using EnvSimulation.Core.Creatures;
using EnvSimulation.Core.Senses;

namespace EnvSimulation.Core.Behaviours
{
    public interface ICreatureBehaviour
    {
        Move GetNextMove(CreatureState state, Species species, CreatureVision vision);
    }
}

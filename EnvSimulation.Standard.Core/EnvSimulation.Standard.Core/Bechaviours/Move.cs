﻿using System;
using System.Collections.Generic;

namespace EnvSimulation.Core.Behaviours
{
    public class Move
    {
        public int Y { get; set; }
        public int X { get; set; }

    }
}

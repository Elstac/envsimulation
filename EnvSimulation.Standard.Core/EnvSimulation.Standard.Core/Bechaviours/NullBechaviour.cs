﻿using EnvSimulation.Core.Creatures;
using EnvSimulation.Core.Senses;

namespace EnvSimulation.Core.Behaviours
{
    class NullBehaviour : ICreatureBehaviour
    {
        public Move GetNextMove(CreatureState state, Species species, CreatureVision vision)
        {
            return new Move { X = 0, Y = 0 };
        }
    }
}

﻿namespace EnvSimulation.Core.Behaviours.PathFinding.DataStructures.Graph
{
    public class Edge
    {
        public int cost;
        public Node target;

        public Edge(int cost,Node target)
        {
            this.target = target;
            this.cost = cost;
        }
    }
}

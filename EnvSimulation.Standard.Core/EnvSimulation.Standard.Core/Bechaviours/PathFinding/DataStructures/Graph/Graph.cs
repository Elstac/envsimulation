﻿using EnvSimulation.Core.Envirnoment;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace EnvSimulation.Core.Behaviours.PathFinding.DataStructures.Graph
{
    class Graph
    {
        List<Node> nodes;
        public int size;

        public Graph(Terrain terrain)
        {
            var size = terrain.GetSize();
            this.size = size.Width*size.Height;
            nodes = new List<Node>();

            for (int x = 0; x < size.Width; x++)
            {
                for (int y = 0; y < size.Height; y++)
                {
                    if(terrain.GetField(x,y).IsPassable())
                        nodes.Add(new Node(new Envirnoment.Position() { X = x, Y = y }));
                }
            }
        }

        public void AddEdge(Position srcPos, Position tarPos, int cost)
        {
            var target = GetNode(tarPos);
            var source = GetNode(srcPos);

            source.edges.Add(new Edge(cost, target));
            target.edges.Add(new Edge(cost, source));
        }

        public bool EdgeExists(Position source, Position target)
        {
            return !(nodes.FirstOrDefault(n => n.position == source && n.edges.Exists(e=>e.target.position == target)) is null);
        }

        public Node GetNode(Position index)
        {
            return nodes.FirstOrDefault(n=>n.position.Equals(index));
        }

        public void AddNode(Position index)
        {
            nodes.Add(new Node(index));
        }
    }
}

﻿namespace EnvSimulation.Core.Communication
{
    public class CreatureState
    {
        public string SpeciesName { get; set; }
        public int SpeciesOrder { get; set; }
        public int Hunger { get; set; }
        public int Health { get; set; }
        public bool InCombat { get; set; }
        public bool Mating { get; set; }
        public string Id { get; set; }
        public string TargetId { get; set; }
    }
}

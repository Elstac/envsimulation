﻿using EnvSimulation.Core.Creatures;
using EnvSimulation.Core.Envirnoment;
using EnvSimulation.Core.Envirnoment.Elements;
using System.Collections.Generic;
using System.Linq;

namespace EnvSimulation.Core.Communication
{
    public static class WorldStateMapper
    {
        public static FieldState MapField(TerrainField field)
        {
            var terrainType = field.Value == TerrainValue.Obstacle ? 1 : 0;
            var fieldState = new FieldState { TerrainType = terrainType };

            fieldState.Elements = field.Elements.Select(e =>
            {
                if (e is Food)
                    return new ElementState { Name = "Food" };

                return new ElementState { Name = "None"};
            })
            .ToArray();

            return fieldState;
        }

        public static WorldState MapWorld(
            Terrain terrain,
            IEnumerable<Creature> creatureCollection,
            Dictionary<string, int> speciesDict)
        {
            var size = terrain.GetSize();
            var array = new FieldState[size.Width, size.Height];

            for (int x = 0; x < size.Width; x++)
            {
                for (int y = 0; y < size.Height; y++)
                {
                    array[x, y] = MapField(terrain.GetField(x, y));
                }
            }

            foreach (var creature in creatureCollection)
            {
                var position = creature.Position;
                var state = creature.State;

                array[position.X, position.Y].Creature = new CreatureState
                {
                    SpeciesName = creature.SpeciesName,
                    SpeciesOrder = speciesDict[creature.SpeciesName],
                    Hunger = state.Hunger,
                    Health = state.Health,
                    InCombat = creature.InCombat,
                    Mating = creature.Mating,
                    Id = creature.Id.ToString(),
                    TargetId = creature.TargetId.ToString()
                };
            }

            return new WorldState { Fields = array };
        }
    }
}

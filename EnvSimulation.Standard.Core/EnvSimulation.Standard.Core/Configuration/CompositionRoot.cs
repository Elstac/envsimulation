﻿using Autofac;
using EnvSimulation.Core.Configuration;
using EnvSimulation.Shared.Configuration;

namespace EnvSimulation.Standard.Core.Configuration
{
    public static class CompositionRoot
    {
        private static IContainer container;

        public static TClass Resolve<TClass>()
        {
            if (container is null)
                InitializeCompositionRoot();

            return container.Resolve<TClass>();
        }

        public static void InitializeCompositionRoot()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new DIModule());

            builder.RegisterType<OptionsProvider>().AsImplementedInterfaces().AsSelf().SingleInstance();
            builder.Register(ctx => container);

            container = builder.Build();
        }
    }
}

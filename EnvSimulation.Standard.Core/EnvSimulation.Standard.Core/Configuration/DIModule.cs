﻿using Autofac;
using EnvSimulation.Core.Envirnoment.WorldGeneration;
using EnvSimulation.Core.Envirnoment.WorldGeneration.Creatures;
using EnvSimulation.Core.Envirnoment.WorldGeneration.Terrains;
using EnvSimulation.Shared.Configuration;

namespace EnvSimulation.Core.Configuration
{
    public class DIModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<SimulationEngine>().AsImplementedInterfaces();

            builder.Register<ITerrainFactory>(ctx =>
            {
                var options = ctx.Resolve<IOptionsProvider>().GetOptions<WorldGenerationOptions>();

                switch (options.TerrainType)
                {
                    case "plains":
                        return new PlainsFactory(options.WorldSize,options.FoodDensity);
                    case "empty":
                        return new EmptyTerrainFactory(options.WorldSize, options.FoodDensity);
                    case "mountains":
                        return new MountainFactory(options.WorldSize, options.FoodDensity);
                    case "test":
                        return new TestTerrainFactory();
                    default:
                        return new EmptyTerrainFactory(options.WorldSize, options.FoodDensity);
                }
            });

            builder.Register<ICreatureFactory>(ctx =>
            {
                var options = ctx.Resolve<IOptionsProvider>().GetOptions<WorldGenerationOptions>();
                var spCount = options.SpeciesNumber;
                var crCount = options.CreaturePerSpecies;

                switch (options.Behaviour)
                {
                    case "passive":
                        return new PassiveCreatureFactory(spCount,crCount);
                    case "aggressive":
                        return new AggressiveCreatureFactory(spCount, crCount);
                    case "test":
                        return new TestCreatureFactory();
                    default:
                        return new PassiveCreatureFactory(spCount, crCount);
                }
            });
        }
    }
}

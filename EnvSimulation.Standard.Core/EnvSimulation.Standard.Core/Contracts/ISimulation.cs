﻿using EnvSimulation.Core.Communication;
using EnvSimulation.Core.Envirnoment;

namespace EnvSimulation.Core.Contracts
{
    public interface ISimulation
    {
        WorldState GetWorldState();
        void NextTick();
        void RemoveElementsFromField(Position position);
    }
}

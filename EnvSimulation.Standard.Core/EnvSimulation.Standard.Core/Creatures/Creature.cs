﻿using EnvSimulation.Core.Behaviours;
using EnvSimulation.Core.Envirnoment;
using EnvSimulation.Core.Senses;
using System;

namespace EnvSimulation.Core.Creatures
{
    public class Creature
    {
        private ICreatureBehaviour Behaviour;
        private ISense sense;

        private CreatureState state;
        private Position position;
        private Species species;

        private int attack;

        private bool mating;
        private bool inCombat;

        private bool alive;

        private Creature target;

        public delegate void OnDeathEventHandler(Creature sender);
        public event OnDeathEventHandler OnDeath;

        public Position Position { get => new Position { X = position.X, Y = position.Y }; }
        public string SpeciesName { get => species.Name; }
        public CreatureState State { get => state; }
        public bool InCombat { get => inCombat; }
        public bool Mating { get => mating; }
        public Guid Id { get; }
        public Guid TargetId { get => target is null?Guid.Empty:target.Id; }

        public Creature(ICreatureBehaviour Behaviour, ISense sense, Position position, Species species)
        {
            Id = Guid.NewGuid();
            this.Behaviour = Behaviour;
            this.sense = sense;

            mating = false;
            inCombat = false;
            alive = true;

            state = new CreatureState();
            state.NegativeStateEvent += () => 
            {
                EndCombat();
                OnDeath(this);
                alive = false; 
            };

            this.position = position;
            this.species = species;

            attack = 1;
        }

        public void InteractWithCreature(Creature otherCreature)
        {
            if (otherCreature.species != species)
            {
                if (otherCreature.target is null && target is null)
                {
                    inCombat = true;
                    target = otherCreature;

                    otherCreature.inCombat = true;
                    otherCreature.target = this;
                }

                otherCreature.ReciveDamage(attack);
            }
        }

        public Move NextStep(Terrain terrain)
        {
            if(!alive) 
                return new Move { X = 0, Y = 0 };

            ApplyNeeds();

            if (target != null)
            {
                InteractWithCreature(target);
                return new Move { X = 0, Y = 0 };
            }

            var creatureVision = sense.GetVision(terrain, position);
            var movement = Behaviour.GetNextMove(state, species, creatureVision);

            return movement;
        }

        public void Move(Move movement)
        {
            position.X += movement.X;
            position.Y += movement.Y;
        }

        public void ReciveDamage(int damage)
        {
            state.DealDamage(damage);
        }

        public void EndCombat()
        {
            inCombat = false;
            target.inCombat = false;
            target.target = null;
            target = null;
        }

        private void ApplyNeeds()
        {
            state.NextStep();
        }

    }
}

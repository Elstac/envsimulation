﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace EnvSimulation.Core.Creatures
{
    public class Species:IEquatable<Species>
    {
        private string name;

        public Species(string name)
        {
            this.name = name;
        }

        public string Name { get => name; }

        public bool Equals(Species other)
        {
            if (other is null)
                return false;

            return name == other.name;
        }
    }
}

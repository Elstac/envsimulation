﻿using EnvSimulation.Core.Creatures;
using EnvSimulation.Standard.Core.Envirnoment;
using System;

namespace EnvSimulation.Core.Envirnoment.Elements
{
    public class Food : IInteractor
    {
        public event OnDestroyEventHandler.D OnDestroy;

        public void Interact(Creature interactor)
        {
            interactor.State.Feed(10);
            OnDestroy(this);
        }
    }
}

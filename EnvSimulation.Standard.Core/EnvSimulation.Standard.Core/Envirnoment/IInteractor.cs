﻿using EnvSimulation.Core.Creatures;
using EnvSimulation.Standard.Core.Envirnoment;

namespace EnvSimulation.Core.Envirnoment
{
    public interface IInteractor
    {
        void Interact(Creature interactor);

        event OnDestroyEventHandler.D OnDestroy;
    }
}

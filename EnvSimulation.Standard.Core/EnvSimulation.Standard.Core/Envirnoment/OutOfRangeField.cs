﻿namespace EnvSimulation.Core.Envirnoment
{
    public class OutOfRangeField : TerrainField
    {
        public OutOfRangeField() : base(TerrainValue.Obstacle)
        {
        }

        public override bool IsPassable()
        {
            return false;
        }
    }
}

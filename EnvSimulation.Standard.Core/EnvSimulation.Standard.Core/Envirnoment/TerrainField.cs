﻿using EnvSimulation.Core.Creatures;
using EnvSimulation.Core.Envirnoment.Elements;
using System.Collections.Generic;
using System.Linq;

namespace EnvSimulation.Core.Envirnoment
{
    public class TerrainField
    {
        public TerrainField(TerrainValue value)
        {
            Value = value;
            elements = new List<IInteractor>();
            creature = null;
        }

        private List<IInteractor> elements;
        private Creature creature;

        public IReadOnlyCollection<IInteractor> Elements { get => elements.AsReadOnly(); }

        public Creature Creature { get => creature; }
        public TerrainValue Value { get; set; }
        public Position Position { get; set; }

        public override bool Equals(object obj)
        {
            if(obj is TerrainField)
            {
                var field2 = obj as TerrainField;
                return Value == field2.Value;
            }

            return base.Equals(obj);
        }

        public virtual bool IsPassable()
        {
            return Value != TerrainValue.Obstacle && Creature is null;
        }

        public bool HasFood()
        {
            return elements.FirstOrDefault(e => e is Food) != null;
        }

        public bool IsEmpty()
        {
            return elements.Count == 0 && creature is null;
        }

        internal bool RemoveInteractor(IInteractor interactor)
        {
            return elements.Remove(interactor);
        }

        internal void ClearInteractors()
        {
            elements.Clear();
        }

        internal void AddInteractor(IInteractor interactor)
        {
            elements.Add(interactor);
        }

        internal void AddCreature(Creature creature)
        {
            this.creature = creature;
        }

        internal void RemoveCreature()
        {
            creature = null;
        }
    }
}

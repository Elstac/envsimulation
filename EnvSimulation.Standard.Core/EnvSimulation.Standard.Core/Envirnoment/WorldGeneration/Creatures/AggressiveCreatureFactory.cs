﻿using EnvSimulation.Core.Behaviours;
using EnvSimulation.Shared.RNG;

namespace EnvSimulation.Core.Envirnoment.WorldGeneration.Creatures
{
    class AggressiveCreatureFactory : CreatureFactory
    {
        public AggressiveCreatureFactory(int speciesNumber, int creaturePerSpecies) : base(speciesNumber, creaturePerSpecies)
        {
        }

        protected override ICreatureBehaviour GetBehaviour()
        {
            return new AggressiveBehaviour(new SimpleBehaviour(new RandomBehaviour(new RandomGenerator())));
        }
    }
}

﻿using EnvSimulation.Core.Behaviours;
using EnvSimulation.Core.Creatures;
using EnvSimulation.Core.Senses;
using EnvSimulation.Shared.RNG;
using System;
using System.Collections.Generic;

namespace EnvSimulation.Core.Envirnoment.WorldGeneration.Creatures
{
    abstract class CreatureFactory:ICreatureFactory
    {
        private int speciesNumber;
        private int creaturePerSpecies;

        public CreatureFactory(int speciesNumber, int creaturePerSpecies)
        {
            this.speciesNumber = speciesNumber;
            this.creaturePerSpecies = creaturePerSpecies;
        }

        public List<Creature> GetSpeciesGroup(Terrain terrain)
        {
            var rng = new Random();

            var avaliableFields = GetAvaliableFields(terrain);

            var Behaviour = GetBehaviour();

            var ret = new List<Creature>();

            for (int i = 0; i < speciesNumber; i++)
            {
                var species = new Species(i.ToString());
                for (int j = 0; j < creaturePerSpecies; j++)
                {
                    var randomIndex = rng.Next(0, avaliableFields.Count);

                    var field = avaliableFields[randomIndex];
                    avaliableFields.RemoveAt(randomIndex);

                    var creature = new Creature(Behaviour, new VisionSense(6), field.Position, species);
                    field.AddCreature(creature);

                    ret.Add(creature);
                }
            }

            return ret;
        }

        private List<TerrainField> GetAvaliableFields(Terrain terrain)
        {
            var terrainSize = terrain.GetSize();
            var ret = new List<TerrainField>();

            for (int x = 0; x < terrainSize.Width; x++)
            {
                for (int y = 0; y < terrainSize.Height; y++)
                {
                    var field = terrain.GetField(x, y);

                    if (field.IsPassable() && field.IsEmpty())
                        ret.Add(field);
                }
            }

            return ret;
        }

        protected abstract ICreatureBehaviour GetBehaviour();
    }
}

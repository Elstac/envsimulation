﻿using EnvSimulation.Core.Behaviours;
using EnvSimulation.Core.Creatures;
using EnvSimulation.Core.Senses;
using EnvSimulation.Shared.RNG;
using System.Collections.Generic;

namespace EnvSimulation.Core.Envirnoment.WorldGeneration.Creatures
{
    class TestCreatureFactory : ICreatureFactory
    {
        public List<Creature> GetSpeciesGroup(Terrain terrain)
        {
            return new List<Creature>
            {
                new Creature(new AggressiveBehaviour(new RandomBehaviour(new RandomGenerator())),
                new VisionSense(6),
                new Position{X=0,Y=0 },
                new Species("test")),
                new Creature(new AggressiveBehaviour(new RandomBehaviour(new RandomGenerator())),
                new VisionSense(6),
                new Position{X=2,Y=0 },
                new Species("test2"))
            };
        }
    }
}

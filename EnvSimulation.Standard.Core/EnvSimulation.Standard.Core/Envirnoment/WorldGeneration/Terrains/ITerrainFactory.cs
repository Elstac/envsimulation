﻿using EnvSimulation.Standard.Core.Envirnoment;

namespace EnvSimulation.Core.Envirnoment.WorldGeneration
{
    public interface ITerrainFactory
    {
        Terrain GetTerrain(OnDestroyEventHandler.D foodHandler);
    }
}

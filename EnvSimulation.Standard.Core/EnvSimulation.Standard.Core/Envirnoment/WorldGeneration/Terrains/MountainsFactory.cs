﻿using System;

namespace EnvSimulation.Core.Envirnoment.WorldGeneration
{
    class MountainFactory : TerrainFactory
    {
        private Random rng;
        private const double baseObstacleProp = 0.2f;
        private const double obstacleAdjBonus = 0.1f;

        public MountainFactory(string terrainSize, string foodDensity) : base(terrainSize, foodDensity)
        {
            rng = new Random();
        }

        protected override Terrain GenerateTerrain(int width, int height)
        {
            var terrain = new TerrainBuilder().BuildEmptyTerrain(width, height).Build();

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    var field = terrain.GetField(x, y);

                    var roll = rng.NextDouble();
                    var obstacleProp = GetObstaclePropability(terrain, x, y);

                    if (roll < obstacleProp)
                        field.Value = TerrainValue.Obstacle;
                }
            }

            return terrain;
        }

        private double GetObstaclePropability(Terrain terrain, int x, int y)
        {
            var propability = baseObstacleProp;
            var fieldsToCheck = terrain.GetAdjecentFields(x, y);

            foreach (var field in fieldsToCheck)
            {
                if (field.Value == TerrainValue.Obstacle)
                    propability += obstacleAdjBonus;
            }

            return propability;
        }
    }
}

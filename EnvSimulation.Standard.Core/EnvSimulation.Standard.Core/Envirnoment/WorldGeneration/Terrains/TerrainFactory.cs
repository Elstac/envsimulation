﻿using EnvSimulation.Core.Envirnoment.Elements;
using EnvSimulation.Standard.Core.Envirnoment;
using System;

namespace EnvSimulation.Core.Envirnoment.WorldGeneration
{
    abstract class TerrainFactory : ITerrainFactory
    {
        private Func<Terrain> factoryMethod;
        private float foodProp;

        public TerrainFactory(string terrainSize,string foodDensity)
        {
            switch (terrainSize)
            {
                case "small":
                    factoryMethod = () => GenerateTerrain(16, 8);
                    break;
                case "medium":
                    factoryMethod = () => GenerateTerrain(24, 12);
                    break;
                case "big":
                    factoryMethod = () => GenerateTerrain(32, 16);
                    break;
                default:
                    factoryMethod = () => GenerateTerrain(24, 12);
                    break;
            }

            switch (foodDensity)
            {
                case "low":
                    foodProp = 0.05f;
                    break;
                case "medium":
                    foodProp = 0.1f;
                    break;
                case "high":
                    foodProp = 0.5f;
                    break;
                default:
                    foodProp = 0.1f;
                    break;
            }
        }

        public Terrain GetTerrain(OnDestroyEventHandler.D foodHandler)
        {
            var terrain = factoryMethod();
            var size = terrain.GetSize();
            var rng = new Random();

            for (int x = 0; x < size.Width; x++)
            {
                for (int y = 0; y < size.Height; y++)
                {
                    var roll = rng.NextDouble();
                    if (roll < foodProp)
                    {
                        var food = new Food();
                        terrain.GetField(x, y).AddInteractor(food);
                        food.OnDestroy += foodHandler;
                    }
                }
            }

            return terrain;
        }

        protected abstract Terrain GenerateTerrain(int width, int height);
    }
}

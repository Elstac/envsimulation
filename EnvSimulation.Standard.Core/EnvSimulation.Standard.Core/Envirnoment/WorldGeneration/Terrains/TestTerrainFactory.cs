﻿using EnvSimulation.Core.Envirnoment.Elements;
using EnvSimulation.Standard.Core.Envirnoment;

namespace EnvSimulation.Core.Envirnoment.WorldGeneration.Terrains
{
    class TestTerrainFactory : ITerrainFactory
    {
        public Terrain GetTerrain(OnDestroyEventHandler.D handler)
        {
            return new TerrainBuilder()
                .BuildEmptyTerrain(20, 10)
                //.WithCustomInteractor(new Food(),2,1)
                .WithCustomTileType(TerrainValue.Obstacle,1,0)
                .WithCustomTileType(TerrainValue.Obstacle,1,1)
                .WithCustomTileType(TerrainValue.Obstacle,0,1)
                .Build();
        }
    }
}

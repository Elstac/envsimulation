﻿using EnvSimulation.Core.Envirnoment.WorldGeneration.Creatures;
using System.Collections.Generic;

namespace EnvSimulation.Core.Envirnoment.WorldGeneration
{
    class WorldGenerator
    {
        private ITerrainFactory terrainFactory;
        private ICreatureFactory creatureFactory;
        private string worldSize;

        private Dictionary<string, int> creatureLimitDictionary = new Dictionary<string, int>
        {
            {"small", 5 },
            {"medium", 10 },
            {"big", 20 },
        };

        public WorldGenerator(ITerrainFactory terrainFactory, ICreatureFactory creatureFactory,string worldSize)
        {
            this.terrainFactory = terrainFactory;
            this.creatureFactory = creatureFactory;
            this.worldSize = worldSize;
        }

        //public List<Creature> GetCreatures(int numberOfSpecies)
        //{
        //    var creatureCollection = new List<Creature>();
        //    var creatureLimit = creatureLimitDictionary[worldSize];
        //    var speciesCreatureLimit = creatureLimit / numberOfSpecies;

        //    for (int i = 0; i < numberOfSpecies; i++)
        //    {
        //        for (int j = 0; j < speciesCreatureLimit; j++)
        //        {
        //            var creature = creatureFactory.GetSpeciesGroup(i);
        //            creatureCollection.Add(creature);
        //        }
        //    }

        //    return creatureCollection;
        //}
    }
}

﻿using EnvSimulation.Core.Envirnoment;
using EnvSimulation.Shared;
using EnvSimulation.Shared.Helpers;
using System;

namespace EnvSimulation.Core.Senses
{
    public class VisionSense : ISense
    {
        private int range;

        public VisionSense()
        {

        }

        public VisionSense(int range)
        {
            this.range = range;
        }

        public CreatureVision GetVision(Terrain terrain,Position position)
        {
            if (terrain is null)
                throw new InvalidStateException("Terrain cannot be null");

            if (range == 0)
                return CreatureVision.CreateEmpty();

            var terrainFields = GetFieldsFromTerrain(terrain, position);

            return new CreatureVision(terrainFields);
        }

        private TerrainField[,] GetFieldsFromTerrain(Terrain terrain, Position position)
        {
            var efectiveRange = range * 2 + 1;
            var terrainArray = new TerrainField[efectiveRange, efectiveRange];

            var beginX = position.X - range;
            var beginY = position.Y - range;

            for (int x = beginX; x < beginX + efectiveRange; x++)
            {
                for (int y = beginY; y < beginY + efectiveRange; y++)
                {
                    if (x < 0 || y < 0 || x >= terrain.GetSize().Width || y >= terrain.GetSize().Height)
                    {
                        terrainArray[x - beginX, y - beginY] = new OutOfRangeField();
                        continue;
                    }

                    if (DistanceHelper.InRange(x, y, position.X, position.Y, range))
                        terrainArray[x - beginX, y - beginY] = terrain.GetField(x, y);
                    else
                        terrainArray[x - beginX, y - beginY] = new OutOfRangeField();
                }
            }

            return terrainArray;
        }
    }
}

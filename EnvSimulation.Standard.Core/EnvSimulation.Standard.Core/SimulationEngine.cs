﻿using EnvSimulation.Core.Communication;
using EnvSimulation.Core.Contracts;
using EnvSimulation.Core.Creatures;
using EnvSimulation.Core.Envirnoment;
using EnvSimulation.Core.Envirnoment.Elements;
using EnvSimulation.Core.Envirnoment.WorldGeneration;
using EnvSimulation.Core.Envirnoment.WorldGeneration.Creatures;
using System.Collections.Generic;

namespace EnvSimulation.Core
{
    public class SimulationEngine: ISimulation
    {
        private Terrain terrain;
        private List<Creature> creatures;

        private Queue<IInteractor> toDestroyQueue;
        private Queue<Creature> toKillQueue;

        private Dictionary<string, int> speciesDict;

        public SimulationEngine(ICreatureFactory creatureFactory, ITerrainFactory terrainFactory)
        {
            speciesDict = new Dictionary<string, int>();
            toDestroyQueue = new Queue<IInteractor>();
            toKillQueue = new Queue<Creature>();

            terrain = terrainFactory.GetTerrain(sender => toDestroyQueue.Enqueue(sender));
            creatures = creatureFactory.GetSpeciesGroup(terrain);

            var speciesCounter = 0;
            foreach (var creature in creatures)
            {
                if (!speciesDict.ContainsKey(creature.SpeciesName))
                    speciesDict.Add(creature.SpeciesName, ++speciesCounter);

                var position = creature.Position;
                creature.OnDeath += KillCreature;
                terrain.GetField(position.X, position.Y).AddCreature(creature);
            }
        }

        private void KillCreature(Creature cr)
        {
            toKillQueue.Enqueue(cr);

            var food = new Food();
            food.OnDestroy += f => toDestroyQueue.Enqueue(f);

            terrain.GetField(cr.Position.X, cr.Position.Y).AddInteractor(food);
        }

        public WorldState GetWorldState()
        {
            return WorldStateMapper.MapWorld(terrain,creatures,speciesDict);
        }

        public void NextTick()
        {

            foreach (var creature in creatures)
            {
                var movement = creature.NextStep(terrain);
                var field = terrain.GetField(creature.Position.X + movement.X, creature.Position.Y + movement.Y);

                var creatuerOnTarget = field.Creature;

                if (!(creatuerOnTarget is null))
                {
                    creature.InteractWithCreature(creatuerOnTarget);
                }
                else
                {
                    if(field.Elements.Count > 0)
                    {
                        foreach (var element in field.Elements)
                        {
                            element.Interact(creature);
                        }
                    }

                    if (field.IsPassable())
                    {
                        var currentField = terrain.GetField(creature.Position.X, creature.Position.Y);

                        currentField.RemoveCreature();
                        creature.Move(movement);
                        field.AddCreature(creature);
                    }
                }
                DestroyAwaitingInteractors();
            }

            while (toKillQueue.Count > 0)
            {
                var toKill = toKillQueue.Dequeue();
                creatures.Remove(toKill);

                terrain.GetField(toKill.Position.X, toKill.Position.Y).RemoveCreature();
            }
        }

        private void DestroyAwaitingInteractors()
        {
            while(toDestroyQueue.Count > 0)
            {
                var toRemove = toDestroyQueue.Dequeue();

                terrain.RemoveElement(toRemove);
            }
        }
        

        public void RemoveElementsFromField(Position position)
        {
            terrain.RemoveElements(position);
        }
    }
}

﻿namespace EnvSimulation.Shared.Configuration
{
    public class WorldGenerationOptions:IOptionSet
    {
        public string WorldSize { get; set; }
        public string WorldShape { get; set; }
        public string TerrainType { get; set; }
        public string FoodDensity { get; set; }

        public string Behaviour { get; set; }
        public int SpeciesNumber { get; set; }
        public string GlobalEnergyConsumption { get; set; }
        public int CreaturePerSpecies { get; set; }
    }
}

﻿using System;
using System.Text;

namespace EnvSimulation.Shared.Helpers
{
    public static class RandomStringGenerator
    {
        public static string GetRandomString(int length)
        {
            var sb = new StringBuilder();
            var rng = new Random();

            for (int i = 0; i < length; i++)
                sb.Append(rng.Next(65, 90));

            return sb.ToString();
        }
    }
}

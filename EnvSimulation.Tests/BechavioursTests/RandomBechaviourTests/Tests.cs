﻿//using EnvSimulation.Core.Behaviours;
//using EnvSimulation.Core.Envirnoment;
//using EnvSimulation.Shared.RNG;
//using Moq;
//using System;
//using Xunit;

//namespace EnvSimulation.Tests.BehavioursTests.RandomBehaviourTests
//{
//    public class Tests:IDisposable
//    {
//        private int step = 0;

//        public Tests()
//        {
//            step = 0;
//        }

//        public void Dispose()
//        {
//            step = 0;
//        }

//        [Fact]
//        public void Get_random_move_if_whole_terrain_is_accesible()
//        {
//            var generatorMock = GetGeneratorWithSequence(new int[] {1});

//            var Behaviour = new RandomBehaviour(generatorMock.Object);
//            var state = new CreatureStateBuilder().Build();
//            var terrain = new TerrainBuilder()
//                .BuildRandomTerrain(3)
//                .Build();
//            var vision = VisionFactory.CreateVisionBasedOnTerrain(terrain);

//            var move = Behaviour.GetNextMove(state, vision);

//            Assert.True(move.X != 0 || move.Y != 0);
//        }

//        [Fact]
//        public void Return_horizontal_move_if_generator_returns_one()
//        {
//            var generatorMock = GetGeneratorWithSequence(new int[] { 1 });

//            var Behaviour = new RandomBehaviour(generatorMock.Object);
//            var state = new CreatureStateBuilder().Build();
//            var terrain = new TerrainBuilder()
//                .BuildRandomTerrain(3)
//                .Build();
//            var vision = VisionFactory.CreateVisionBasedOnTerrain(terrain);

//            var move = Behaviour.GetNextMove(state, vision);

//            Assert.True(move.X == 1 || move.Y == 0);
//        }

//        [Fact]
//        public void Return_vartical_move_if_generator_returns_zero_and_then_one()
//        {
//            var generatorMock = GetGeneratorWithSequence(new int[] { 0,1 });

//            var Behaviour = new RandomBehaviour(generatorMock.Object);
//            var state = new CreatureStateBuilder().Build();
//            var terrain = new TerrainBuilder()
//                .BuildRandomTerrain(3)
//                .Build();
//            var vision = VisionFactory.CreateVisionBasedOnTerrain(terrain);

//            var move = Behaviour.GetNextMove(state, vision);

//            Assert.Equal(0, move.X);
//            Assert.Equal(1, move.Y);
//        }

//        [Fact]
//        public void Reroll_moveX_if_first_one_leads_to_obstacle()
//        {
//            var generatorMock = GetGeneratorWithSequence(new int[] { -1, 1 });

//            var Behaviour = new RandomBehaviour(generatorMock.Object);
//            var state = new CreatureStateBuilder().Build();
//            var terrain = new TerrainBuilder()
//                .BuildRandomTerrain(3)
//                .WithCustomTileType(TerrainValue.Obstacle,0,1)
//                .Build();

//            var vision = VisionFactory.CreateVisionBasedOnTerrain(terrain);

//            var move = Behaviour.GetNextMove(state, vision);

//            Assert.Equal(1, move.X);
//        }

//        [Fact]
//        public void Reroll_moveY_if_first_one_leads_to_obstacle()
//        {
//            var generatorMock = GetGeneratorWithSequence(new int[] {0, -1, 1});

//            var Behaviour = new RandomBehaviour(generatorMock.Object);
//            var state = new CreatureStateBuilder().Build();
//            var terrain = new TerrainBuilder()
//                .BuildRandomTerrain(3)
//                .WithCustomTileType(TerrainValue.Obstacle, 1, 0)
//                .Build();

//            var vision = VisionFactory.CreateVisionBasedOnTerrain(terrain);

//            var move = Behaviour.GetNextMove(state, vision);

//            Assert.Equal(1, move.Y);
//        }

//        [Fact]
//        public void Reroll_moveX_until_leads_to_passsable_terrain()
//        {
//            var generatorMock = GetGeneratorWithSequence(new int[] { -1, -1, 1});

//            var Behaviour = new RandomBehaviour(generatorMock.Object);
//            var state = new CreatureStateBuilder().Build();
//            var terrain = new TerrainBuilder()
//                .BuildRandomTerrain(3)
//                .WithCustomTileType(TerrainValue.Obstacle, 0, 1)
//                .Build();

//            var vision = VisionFactory.CreateVisionBasedOnTerrain(terrain);

//            var move = Behaviour.GetNextMove(state, vision);

//            Assert.Equal(1, move.X);
//        }

//        [Fact]
//        public void Change_direction_if_reroll_after_detecting_obstacle_returns_zero()
//        {
//            var generatorMock = GetGeneratorWithSequence(new int[] { -1, 0, 1 });

//            var Behaviour = new RandomBehaviour(generatorMock.Object);
//            var state = new CreatureStateBuilder().Build();
//            var terrain = new TerrainBuilder()
//                .BuildRandomTerrain(3)
//                .WithCustomTileType(TerrainValue.Obstacle, 0, 1)
//                .Build();

//            var vision = VisionFactory.CreateVisionBasedOnTerrain(terrain);

//            var move = Behaviour.GetNextMove(state, vision);

//            Assert.Equal(1, move.Y);
//            Assert.Equal(0, move.X);
//        }

//        [Fact]
//        public void Never_returns_null_move()
//        {
//            var generatorMock = GetGeneratorWithSequence(new int[] { 0, 0, 1 });

//            var Behaviour = new RandomBehaviour(generatorMock.Object);
//            var state = new CreatureStateBuilder().Build();
//            var terrain = new TerrainBuilder()
//                .BuildRandomTerrain(3)
//                .WithCustomTileType(TerrainValue.Obstacle, 0, 1)
//                .Build();

//            var vision = VisionFactory.CreateVisionBasedOnTerrain(terrain);

//            var move = Behaviour.GetNextMove(state, vision);

//            Assert.True(move.X != 0 || move.Y != 0);
//        }

//        private Mock<IRandomGenerator> GetGeneratorWithSequence(int [] sequence)
//        {
//            var mock = new Mock<IRandomGenerator>();
//            mock
//                .Setup(m => m.GetRandomInt(It.IsAny<int>(), It.IsAny<int>()))
//                .Returns(()=>
//                {
//                    var mod = step++ % sequence.Length;

//                    return sequence[mod];
//                });

//            return mock;
//        }
//    }
//}

﻿//using EnvSimulation.Core.Behaviours;
//using EnvSimulation.Core.Envirnoment;
//using EnvSimulation.Core.Envirnoment.Elements;
//using EnvSimulation.Core.Senses;
//using Xunit;

//namespace EnvSimulation.Tests.BehavioursTests.SimpleBehaviourTests
//{
//    public class Tests
//    {
//        [Fact]
//        public void When_vison_is_empty_creature_stays()
//        {
//            var Behaviour = new SimpleBehaviour();
//            var state = new CreatureStateBuilder().Build();

//            var move = Behaviour.GetNextMove(state,CreatureVision.CreateEmpty());

//            Assert.Equal(0, move.X);
//            Assert.Equal(0, move.Y);
//        }

//        [Theory]
//        [InlineData(7,3,4)]
//        [InlineData(5,2,3)]
//        [InlineData(3,1,2)]
//        public void Move_one_down_if_food_is_bottom_in_vision(int size, int foodPosX, int foodPosY)
//        {
//            var Behaviour = new SimpleBehaviour();
//            var state = new CreatureStateBuilder().Build();
//            var terrain = new TerrainBuilder()
//                .BuildRandomTerrain(size)
//                .WithCustomInteractor(new Food(), foodPosX, foodPosY)
//                .Build();
//            var vision = VisionFactory.CreateVisionBasedOnTerrain(terrain);

//            var move = Behaviour.GetNextMove(state, vision);

//            Assert.Equal(0, move.X);
//            Assert.Equal(1, move.Y);
//        }

//        [Fact]
//        public void Move_one_left_if_food_is_on_left_in_vision_with_size_of_5()
//        {
//            var Behaviour = new SimpleBehaviour();
//            var state = new CreatureStateBuilder().Build();
//            var terrain = new TerrainBuilder()
//                .BuildRandomTerrain(5)
//                .WithCustomInteractor(new Food(), 1, 2)
//                .Build();
//            var vision = VisionFactory.CreateVisionBasedOnTerrain(terrain);

//            var move = Behaviour.GetNextMove(state, vision);

//            Assert.Equal(-1, move.X);
//            Assert.Equal(0, move.Y);
//        }

//        [Fact]
//        public void Move_one_right_if_food_is_on_right_in_vision_with_size_of_5()
//        {
//            var Behaviour = new SimpleBehaviour();
//            var state = new CreatureStateBuilder().Build();
//            var terrain = new TerrainBuilder()
//                .BuildRandomTerrain(5)
//                .WithCustomInteractor(new Food(), 3, 2)
//                .Build();
//            var vision = VisionFactory.CreateVisionBasedOnTerrain(terrain);

//            var move = Behaviour.GetNextMove(state, vision);

//            Assert.Equal(1, move.X);
//            Assert.Equal(0, move.Y);
//        }

//        [Theory]
//        [InlineData(0,2,-1,0)]
//        [InlineData(4,2,1,0)]
//        [InlineData(2,4,0,1)]
//        [InlineData(2,0,0,-1)]
//        public void Move_toward_food_if_food_is_2_tiles_away(int foodX, int foodY, int directionX, int directionY)
//        {
//            var Behaviour = new SimpleBehaviour();
//            var state = new CreatureStateBuilder().Build();
//            var terrain = new TerrainBuilder()
//                .BuildRandomTerrain(5)
//                .WithCustomInteractor(new Food(), foodX, foodY)
//                .Build();
//            var vision = VisionFactory.CreateVisionBasedOnTerrain(terrain);

//            var move = Behaviour.GetNextMove(state, vision);

//            Assert.Equal(directionX, move.X);
//            Assert.Equal(directionY, move.Y);
//        }

//        [Fact]
//        public void Move_up_if_food_is_on_upright_corner()
//        {
//            var Behaviour = new SimpleBehaviour();
//            var state = new CreatureStateBuilder().Build();
//            var terrain = new TerrainBuilder()
//                .BuildRandomTerrain(5)
//                .WithCustomInteractor(new Food(), 3, 1)
//                .Build();
//            var vision = VisionFactory.CreateVisionBasedOnTerrain(terrain);

//            var move = Behaviour.GetNextMove(state, vision);

//            Assert.Equal(0, move.X);
//            Assert.Equal(-1, move.Y);
//        }

//        [Fact]
//        public void Move_down_if_food_is_on_downright_corner()
//        {
//            var Behaviour = new SimpleBehaviour();
//            var state = new CreatureStateBuilder().Build();
//            var terrain = new TerrainBuilder()
//                .BuildRandomTerrain(5)
//                .WithCustomInteractor(new Food(), 3, 3)
//                .Build();
//            var vision = VisionFactory.CreateVisionBasedOnTerrain(terrain);

//            var move = Behaviour.GetNextMove(state, vision);

//            Assert.Equal(0, move.X);
//            Assert.Equal(1, move.Y);
//        }
//    }
//}

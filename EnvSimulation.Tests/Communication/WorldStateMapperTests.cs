﻿//using EnvSimulation.Core.Communication;
//using EnvSimulation.Core.Creatures;
//using EnvSimulation.Core.Envirnoment;
//using EnvSimulation.Core.Envirnoment.Elements;
//using System.Linq;
//using Xunit;

//namespace EnvSimulation.Tests.Communication
//{
//    public class WorldStateMapperTests
//    {
//        [Fact]
//        public void Map_empty_terrain_to_0()
//        {
//            //Arrange
//            var terrainField = new TerrainField(TerrainValue.Ground);

//            //Act
//            var field = WorldStateMapper.MapField(terrainField).TerrainType;

//            //Assert
//            Assert.Equal(0, field);
//        }

//        [Fact]
//        public void Map_obstacle_to_1()
//        {
//            //Arrange
//            var terrainField = new TerrainField(TerrainValue.Obstacle);

//            //Act
//            var field = WorldStateMapper.MapField(terrainField).TerrainType;

//            //Assert
//            Assert.Equal(1, field);
//        }

//        [Theory]
//        [InlineData(1,1)]
//        [InlineData(2,1)]
//        [InlineData(1,2)]
//        public void Map_first_creature_on_given_position_to_1(int x, int y)
//        {
//            //Arrange
//            var terrain = new TerrainBuilder().BuildEmptyTerrain(10).Build();
//            var creature = new CreatureBuilder().WithPosition(x,y).Build();

//            //Act
//            var worldState = WorldStateMapper.MapWorld(terrain, new Creature[] { creature });
//            var filed = worldState.Fields[x,y];
//            //Assert
//            Assert.Equal(1, filed.Creature.SpeciesOrder);
//        }

//        [Theory]
//        [InlineData(2,2)]
//        [InlineData(3,2)]
//        [InlineData(4,4)]
//        public void Map_second_creature_of_the_same_species_on_given_position_to_1(int x, int y)
//        {
//            //Arrange
//            var terrain = new TerrainBuilder().BuildEmptyTerrain(10).Build();
//            var creature = new CreatureBuilder().WithPosition(1,1).OfSpecies("1").Build();
//            var creature2 = new CreatureBuilder().WithPosition(x,y).OfSpecies("1").Build();

//            //Act
//            var worldState = WorldStateMapper.MapWorld(terrain, new Creature[] { creature,creature2 });
//            var filed = worldState.Fields[x,y];
//            //Assert
//            Assert.Equal(1, filed.Creature.SpeciesOrder);
//        }

//        [Theory]
//        [InlineData(2, 2,"2")]
//        [InlineData(3, 2,"2")]
//        [InlineData(3, 2,"spec")]
//        public void Map_creature_of_second_species_on_given_position_to_2(int x, int y, string seconSpeciesName)
//        {
//            //Arrange
//            var terrain = new TerrainBuilder().BuildEmptyTerrain(10).Build();
//            var creature = new CreatureBuilder().WithPosition(1, 1).OfSpecies("1").Build();
//            var creature2 = new CreatureBuilder().WithPosition(x,y).OfSpecies(seconSpeciesName).Build();

//            //Act
//            var worldState = WorldStateMapper.MapWorld(terrain, new Creature[] { creature, creature2 });
//            var filed = worldState.Fields[x,y];

//            //Assert
//            Assert.Equal(2, filed.Creature.SpeciesOrder);
//        }

//        [Fact]
//        public void Map_creature_of_third_species_on_given_position_to_3()
//        {
//            //Arrange
//            var terrain = new TerrainBuilder().BuildEmptyTerrain(10).Build();
//            var creature = new CreatureBuilder().WithPosition(1, 1).OfSpecies("1").Build();
//            var creature2 = new CreatureBuilder().WithPosition(2,1).OfSpecies("2").Build();
//            var creature3 = new CreatureBuilder().WithPosition(3,3).OfSpecies("3").Build();

//            //Act
//            var worldState = WorldStateMapper.MapWorld(terrain, new Creature[] { creature, creature2,creature3 });
//            var filed = worldState.Fields[3,3];

//            //Assert
//            Assert.Equal(3, filed.Creature.SpeciesOrder);
//        }

//        [Fact]
//        public void Map_food_element_to_1()
//        {
//            //Arrange
//            var terrain = new TerrainBuilder().BuildEmptyTerrain(10).WithCustomInteractor(new Food(),0,0).Build();

//            //Act
//            var worldState = WorldStateMapper.MapWorld(terrain, new Creature[] {});
//            var filed = worldState.Fields[0,0];

//            //Assert
//            Assert.Equal("Food", filed.Elements.FirstOrDefault().Name);
//        }
//    }
//}

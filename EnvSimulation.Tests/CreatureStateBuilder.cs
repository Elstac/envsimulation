﻿using EnvSimulation.Core.Creatures;
using System;

namespace EnvSimulation.Tests
{
    internal class CreatureStateBuilder
    {
        private CreatureState.NegativeStateEventHandler eventHandler;
        private int hunger;

        public CreatureStateBuilder()
        {
            hunger = 100;
            eventHandler = () => { };
        }

        public CreatureStateBuilder WithNegativeEventHandler(CreatureState.NegativeStateEventHandler handler)
        {
            eventHandler = handler;
            return this;
        }

        public CreatureStateBuilder WithCustomHungerValue(int hungerValue)
        {
            hunger = hungerValue;
            return this;
        }

        public CreatureState Build()
        {
            var state = new CreatureState(hunger);
            state.NegativeStateEvent += eventHandler;

            return state;
        }
    }
}

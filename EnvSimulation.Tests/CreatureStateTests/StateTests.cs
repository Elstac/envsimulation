﻿using EnvSimulation.Core.Creatures;
using Xunit;

namespace EnvSimulation.Tests.CreatureStateTests
{
    public class StateTests
    {
        [Fact]
        public void Next_step_lower_hunger_by_1()
        {
            var creatureState = new CreatureStateBuilder().WithCustomHungerValue(10).Build();

            creatureState.NextStep();

            Assert.Equal(9, creatureState.Hunger);
        }

        [Fact]
        public void Default_hunger_equals_100()
        {
            var creatureState = new CreatureState();

            Assert.Equal(100, creatureState.Hunger);
        }

        [Fact]
        public void Next_step_doesnt_call_event_if_new_hunger_value_is_positive()
        {
            bool eventCalled = false;
            var creatureState = new CreatureStateBuilder()
                .WithCustomHungerValue(2)
                .WithNegativeEventHandler(() => eventCalled = true)
                .Build();

            creatureState.NextStep();

            Assert.False(eventCalled);
        }

        [Fact]
        public void If_next_step_lower_hunger_to_0_call_event()
        {
            bool eventCalled = false;
            var creatureState = new CreatureStateBuilder()
                .WithCustomHungerValue(1)
                .WithNegativeEventHandler(() => eventCalled = true)
                .Build();

            creatureState.NextStep();

            Assert.True(eventCalled);
        }
    }
}

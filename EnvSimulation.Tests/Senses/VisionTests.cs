﻿using EnvSimulation.Core.Envirnoment;
using EnvSimulation.Core.Senses;
using EnvSimulation.Shared;
using Xunit;

namespace EnvSimulation.Tests.Senses
{
    public class VisionTests
    {
        [Fact]
        public void Throw_if_terrain_is_null()
        {
            var visionSense = new VisionSense();
            Terrain terrain = null;

            Assert.Throws<InvalidStateException>(() => visionSense.GetVision(terrain, new Position { X = 0, Y = 0 }));
        }

        [Fact]
        public void Return_vision_with_empty_array_if_range_equal_zero()
        {
            var visionSense = new VisionSense(0);
            Terrain terrain = new TerrainBuilder().BuildRandomTerrain(10).Build();

            var vision = visionSense.GetVision(terrain, new Position {X=0,Y=0});

            Assert.Empty(vision.TerrainFields);
        }

        [Fact]
        public void Creature_vision_fileds_out_of_range()
        {
            var terrain = new TerrainBuilder().BuildRandomTerrain(10).Build();
            var visionSense = new VisionSense(1);

            var vision = visionSense.GetVision(terrain, new Position { X = 4, Y = 4 });

            Assert.IsType<OutOfRangeField>(vision.TerrainFields[0, 0]);
            Assert.IsType<OutOfRangeField>(vision.TerrainFields[2, 0]);
            Assert.IsType<OutOfRangeField>(vision.TerrainFields[0, 2]);
            Assert.IsType<OutOfRangeField>(vision.TerrainFields[2, 2]);
        }

        [Fact]
        public void Vision_contains_fields_from_terrain()
        {
            var terrain = new TerrainBuilder().BuildRandomTerrain(10).Build();
            var visionSense = new VisionSense(1);

            var vision = visionSense.GetVision(terrain, new Position { X = 4, Y = 4 });

            Assert.Equal(vision.TerrainFields[1, 0], terrain.GetField(4,3));
            Assert.Equal(vision.TerrainFields[0, 1], terrain.GetField(3,4));
            Assert.Equal(vision.TerrainFields[1, 2], terrain.GetField(4,5));
            Assert.Equal(vision.TerrainFields[1, 2], terrain.GetField(4,5));
        }

        [Fact]
        public void All_fields_out_of_terrain_are_out_of_range_fields_for_negative()
        {
            var terrain = new TerrainBuilder().BuildRandomTerrain(10).Build();
            var visionSense = new VisionSense(1);

            var vision = visionSense.GetVision(terrain, new Position { X = 0, Y = 0 });

            Assert.IsType<OutOfRangeField>(vision.TerrainFields[0, 0]);
            Assert.IsType<OutOfRangeField>(vision.TerrainFields[1, 0]);
            Assert.IsType<OutOfRangeField>(vision.TerrainFields[2, 0]);
            Assert.IsType<OutOfRangeField>(vision.TerrainFields[0, 1]);
            Assert.IsType<OutOfRangeField>(vision.TerrainFields[0, 2]);
        }

        [Fact]
        public void All_fields_out_of_terrain_are_out_of_range_fields_for_positive()
        {
            var terrain = new TerrainBuilder().BuildRandomTerrain(10).Build();
            var visionSense = new VisionSense(1);

            var vision = visionSense.GetVision(terrain, new Position { X = 9, Y = 9 });

            Assert.IsType<OutOfRangeField>(vision.TerrainFields[0, 2]);
            Assert.IsType<OutOfRangeField>(vision.TerrainFields[1, 2]);
            Assert.IsType<OutOfRangeField>(vision.TerrainFields[2, 2]);
            Assert.IsType<OutOfRangeField>(vision.TerrainFields[2, 0]);
            Assert.IsType<OutOfRangeField>(vision.TerrainFields[2, 1]);
        }
    }
}

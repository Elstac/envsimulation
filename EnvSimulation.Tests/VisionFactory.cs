﻿using EnvSimulation.Core.Envirnoment;
using EnvSimulation.Core.Senses;
using System;

namespace EnvSimulation.Tests
{
    static class VisionFactory
    {
        public static CreatureVision CreateVisionBasedOnTerrain(Terrain terrain)
        {
            var size = terrain.GetSize();
            var terrainArray = new TerrainField[size.Width, size.Height];

            for (int x = 0; x < size.Width; x++)
            {
                for (int y = 0; y < size.Height; y++)
                {
                    terrainArray[x, y] = terrain.GetField(x, y);
                }
            }

            return new CreatureVision(terrainArray);
        }
    }
}

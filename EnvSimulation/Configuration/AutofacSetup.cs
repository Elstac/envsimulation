﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
using EnvSimulation.Core.Configuration;
using EnvSimulation.Shared.Configuration;

namespace EnvSimulation.Console.Configuration
{
    class AutofacSetup
    {
        private static IContainer container;
        public static IContainer Container { get => container; }

        public static void ConfigureContainer()
        {
            if (container != null)
                return;

            var builder = new ContainerBuilder();

            builder.RegisterType<Simulation>().AsSelf();
            builder.RegisterType<OptionsProvider>().AsImplementedInterfaces().AsSelf().SingleInstance();
            builder.Register(ctx => Container);

            builder.RegisterModule(new UIModule());
            builder.RegisterModule(new DIModule());

            container = builder.Build();
        }
    }
}

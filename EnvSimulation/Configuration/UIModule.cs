﻿using Autofac;
using EnvSimulation.Console.UI;
using EnvSimulation.Console.UI.FrameElements;
using EnvSimulation.Console.UI.MainMenu;
using EnvSimulation.Console.UI.Management;
using System.Collections.Generic;

namespace EnvSimulation.Console.Configuration
{
    class UIModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.Register<UIManager>(ctx =>
            {
                var uiManager = new UIManager();

                var mainGameScene = BuildMainGameScene();
                var mainMenuScene = BuildMainMenu();

                uiManager.AddScene(mainMenuScene);
                uiManager.AddScene(mainGameScene);

                return uiManager;
            });
        }

        private  UIScene BuildMainMenu()
        {
            var scene = new UIScene();
            var frame = new UIFrame("main_menu_frame", 0, 0, 119,24);
            var menu = new MainMenu("main_menu");

            menu.HorizontalAligment = HorizontalAligment.Center;
            menu.VerticalAligment = VerticalAligment.Center;

            frame.SetContent(menu);
            scene.AddUIFrame(frame);

            return scene;
        }

        private  UIScene BuildMainGameScene()
        {
            var frame = new UIFrame("worldFrame", 0, 2, 48, 18);
            var wv = new WorldView(
                "worldView",
                new Dictionary<int, char>
                {
                    {0, '.'},
                    {1, '#'}
                },
                new Dictionary<string, char>
                {
                    {"None", ' '},
                    {"Food", '@'}
                }
            );
            wv.VerticalAligment = VerticalAligment.Center;
            wv.HorizontalAligment = HorizontalAligment.Center;
            frame.SetContent(wv);

            var speedBar = new UIFrame("xd", 0, 0, 64, 3);
            var speedView = new GameSpeedView("gameSpeedBar")
            {
                HorizontalAligment = HorizontalAligment.Center,
                VerticalAligment = VerticalAligment.Center,
            };
            speedBar.SetContent(speedView);

            var buttonFrame = new UIFrame("", 47, 2, 17, 8);
            var changeSpeedButtonList = new List<UIButton>
            {
                new UIButton("speedUp","Speed Up"),
                new UIButton("speedDown","Speed Down")
            };

            var sbl = new UIButtonList();

            var al = new UIButtonList();
            al.AddInteractiveElement(new UIButton("remove", "Remove"));

            foreach (var button in changeSpeedButtonList)
                sbl.AddInteractiveElement(button);

            var buttonMenu = new UiFlowMenu("actionMenu");
            buttonMenu.AddSubMenu(sbl, "Change speed");
            buttonMenu.AddSubMenu(al, "Actions");

            buttonFrame.SetContent(buttonMenu);

            var statsFrame = new UIFrame("statsFrame", 47, 9, 17, 11);
            var statsView = new StatisticsView("statsView");
            statsFrame.SetContent(statsView);

            var mainGameScene = new UIScene();
            mainGameScene.AddUIFrame(frame);
            mainGameScene.AddUIFrame(speedBar);
            mainGameScene.AddUIFrame(buttonFrame);
            mainGameScene.AddUIFrame(statsFrame);

            return mainGameScene;
        }
    }
}

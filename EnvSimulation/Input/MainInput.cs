﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace EnvSimulation.Console.Input
{
    class MainInput
    {
        private Dictionary<ConsoleKey, Action> keyboardDict;
        private int cursorX = 1;
        private int cursorY = 1;
        private Queue<Action> actionQueue;

        public MainInput()
        {
            keyboardDict = new Dictionary<ConsoleKey, Action>();
            actionQueue = new Queue<Action>();
            DisableConsoleQuickEdit.Go();
        }

        public void BindKey(ConsoleKey key, Action action)
        {
            if (keyboardDict.ContainsKey(key))
                throw new Exception("Key already has binded action");

            keyboardDict.Add(key, action);
        }

        public void ManageKeyboardInput()
        {
            //System.Console.SetCursorPosition(0,20);
            if (!System.Console.KeyAvailable)
                return;

            var key = System.Console.ReadKey().Key;

            if (!keyboardDict.ContainsKey(key))
                return;

            var action = keyboardDict[key];

            lock (actionQueue)
            {
                actionQueue.Enqueue(action);
            }
        }

        public void ExecuteActions()
        {
            lock (actionQueue)
            {
                while (actionQueue.Count > 0)
                {
                    var action = actionQueue.Dequeue();
                    action();
                }
            }
        }

        public void MoveCursor(int moveX, int moveY)
        {
            cursorX += moveX;
            cursorY += moveY;
        }

        public void UpdateCursor()
        {
            System.Console.SetCursorPosition(cursorX, cursorY);
        }


        static class DisableConsoleQuickEdit
        {

            const uint ENABLE_QUICK_EDIT = 0x0040;

            const int STD_INPUT_HANDLE = -10;

            [DllImport("kernel32.dll", SetLastError = true)]
            static extern IntPtr GetStdHandle(int nStdHandle);

            [DllImport("kernel32.dll")]
            static extern bool GetConsoleMode(IntPtr hConsoleHandle, out uint lpMode);

            [DllImport("kernel32.dll")]
            static extern bool SetConsoleMode(IntPtr hConsoleHandle, uint dwMode);

            internal static bool Go()
            {

                IntPtr consoleHandle = GetStdHandle(STD_INPUT_HANDLE);

                uint consoleMode;
                if (!GetConsoleMode(consoleHandle, out consoleMode))
                {
                    return false;
                }

                consoleMode &= ~ENABLE_QUICK_EDIT;

                if (!SetConsoleMode(consoleHandle, consoleMode))
                {
                    return false;
                }

                return true;
            }
        }
    }
}

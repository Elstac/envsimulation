﻿using Autofac;
using EnvSimulation.Console.Configuration;
using EnvSimulation.Console.UI;
using EnvSimulation.Console.UI.FrameElements;
using EnvSimulation.Console.UI.MainMenu;
using EnvSimulation.Console.UI.Management;
using EnvSimulation.Core;
using System.Collections.Generic;

namespace EnvSimulation.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            AutofacSetup.ConfigureContainer();
            var serviceProvider = AutofacSetup.Container;

            var simulation = serviceProvider.Resolve<Simulation>();

            simulation.Run();
        }
    }
}

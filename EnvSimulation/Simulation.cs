﻿using Autofac;
using EnvSimulation.Console.Input;
using EnvSimulation.Console.UI;
using EnvSimulation.Console.UI.FrameElements;
using EnvSimulation.Console.UI.MainMenu;
using EnvSimulation.Console.UI.Management;
using EnvSimulation.Core.Communication;
using EnvSimulation.Core.Contracts;
using EnvSimulation.Shared.Configuration;
using System;
using System.Timers;
namespace EnvSimulation.Console
{
    class Simulation
    {
        private Timer timer;
        private const float frameTime = 17f;
        private int gameSpeed = 100;
        private int currentFrame = 0;
        private bool update = false;
        private bool paused = false;

        private bool running;

        private UIManager uiManager;
        private WorldView worldView;
        private GameSpeedView speedView;
        private StatisticsView statsView;

        private FocusableUIFrameElement focusedElement;
        private MainInput input;
        private WorldState currentState;

        private ISimulation simulation;
        private IContainer serviceProvider;

        public Simulation(UIManager uiManager, IContainer serviceProvider)
        {
            this.uiManager = uiManager;
            this.serviceProvider = serviceProvider; 

            System.Console.CursorVisible = false;

            InitializeKeyBinds();
        }

        private void TurnOnMenu()
        {
            uiManager.ChangeScene(0);

            FocusElement("main_menu");

            uiManager.BindActionToButton("start_button", (sender, args) => InitializeGame());
            uiManager.BindActionToButton("exit_button", (sender, args) => running = false);

            timer = new Timer(frameTime);
            timer.Elapsed += (sender, e) =>
            {
                input.ManageKeyboardInput();
                input.ExecuteActions();
            };
            timer.Start();
        }

        private void InitializeGame()
        {
            System.Console.Clear();
            var worldGenerationOptions = uiManager.FindUIElementOfType<WorldCreationMenu>().GetOptions();
            var optionsProvider = serviceProvider.Resolve<IOptionsProvider>();

            optionsProvider.PutOptions(worldGenerationOptions);
            StartGame();
        }

        private void StartGame()
        {
            uiManager.ChangeScene(1);
            worldView = uiManager.FindUIElementById<WorldView>("worldView");
            speedView = uiManager.FindUIElementById<GameSpeedView>("gameSpeedBar");
            speedView.OnSpeedChange += (newSpeed) =>
            {
                if (newSpeed == 0)
                    paused = true;
                else
                {
                    paused = false;
                    gameSpeed = 140 - 40 * newSpeed;
                }
            };

            statsView = uiManager.FindUIElementById<StatisticsView>("statsView");

            focusedElement = worldView;


            uiManager.BindActionToButton("speedUp", (sender, args) => speedView.ChangeSpeed(1));
            uiManager.BindActionToButton("speedDown", (sender, args) => speedView.ChangeSpeed(-1));
            uiManager.BindActionToButton("remove", (sender, args) => simulation.RemoveElementsFromField(worldView.cursorPosition));

            timer.Stop();
            timer = new Timer(frameTime);
            timer.Elapsed += (sender, e) =>
            {
                if (update == true)
                    return;

                input.ManageKeyboardInput();
                input.ExecuteActions();

                if (!(currentState is null))
                    statsView.ChangeContent(currentState.Fields[worldView.cursorPosition.X, worldView.cursorPosition.Y]);

                if (paused)
                    return;

                if (currentFrame++ < gameSpeed)
                    return;

                update = true;
                simulation.NextTick();
                currentState = simulation.GetWorldState();
                worldView.UpdateWorldState(currentState);
                update = false;
                currentFrame = 0;
            };

            simulation = serviceProvider.Resolve<ISimulation>();
            var worldState = simulation.GetWorldState();
            worldView.UpdateWorldState(worldState);
            timer.Start();
        }

        private void InitializeKeyBinds()
        {
            input = new MainInput();
            input.BindKey(System.ConsoleKey.Escape, () => running = false);
            input.BindKey(System.ConsoleKey.DownArrow, () => focusedElement.MoveCursor(0, 1));
            input.BindKey(System.ConsoleKey.UpArrow, () => focusedElement.MoveCursor(0, -1));
            input.BindKey(System.ConsoleKey.LeftArrow, () => focusedElement.MoveCursor(-1, 0));
            input.BindKey(System.ConsoleKey.RightArrow, () => focusedElement.MoveCursor(1, 0));
            input.BindKey(System.ConsoleKey.Enter, () => focusedElement.ClickCursor());
            input.BindKey(System.ConsoleKey.Q, () => UnFocusElement());
            input.BindKey(System.ConsoleKey.A, () => FocusElement("actionMenu"));
            input.BindKey(System.ConsoleKey.Add, () => speedView.ChangeSpeed(1));
            input.BindKey(System.ConsoleKey.Subtract, () => speedView.ChangeSpeed(-1));
            input.BindKey(System.ConsoleKey.Spacebar, () => speedView.ChangeSpeed(-1000));

            input.BindKey(System.ConsoleKey.T, () => RunTest());
        }

        private void RunTest()
        {
            var optionsProvider = serviceProvider.Resolve<IOptionsProvider>();

            optionsProvider.PutOptions(new WorldGenerationOptions { 
                TerrainType = "test",
                Behaviour = "test"
            });

            StartGame();
        }

        public void FocusElement(string id)
        {
            UnFocusElement();
            focusedElement = uiManager.FindUIElementById<FocusableUIFrameElement>(id);
            focusedElement.Focus();
        }

        public void UnFocusElement()
        {
            if(focusedElement is null)
                return;

            focusedElement.UnFocus();
            focusedElement = worldView;
            worldView.Focus();
        }

        public void Run()
        {
            running = true;
            TurnOnMenu();
            while (running) ;
        }

    }
}

﻿using EnvSimulation.Console.UI.Text;
using System.Collections.Generic;

namespace EnvSimulation.Console.UI
{
    abstract class FocusableUIFrameElement : UIFrameElement
    {
        public FocusableUIFrameElement(string id):base(id)
        {

        }
        public FocusableUIFrameElement():base()
        {

        }

        public abstract void MoveCursor(int moveX, int moveY);
        public abstract void ClickCursor();
        public abstract void Focus();
        public abstract void UnFocus();
    }
}

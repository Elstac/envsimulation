﻿using EnvSimulation.Console.UI.Text;
using System;
using System.Collections.Generic;

namespace EnvSimulation.Console.UI.FrameElements
{
    class GameSpeedView : UIFrameElement
    {
        private List<UIString> timeButtons;
        private int actualSpeed;

        public delegate void SpeedChangeEventHandler(int newGameSpeed);
        public event SpeedChangeEventHandler OnSpeedChange;

        public GameSpeedView(string id):base(id)
        {
            timeButtons = new List<UIString>();
            actualSpeed = 2;

            timeButtons.Add(new UIString("||"));
            timeButtons.Add(new UIString("|>"));
            timeButtons.Add(new UIString(">"));
            timeButtons.Add(new UIString(">>"));

            FocusButton(actualSpeed);
        }

        public override UITextArray Draw()
        {
            var textArray = new UITextArray();

            foreach (var item in timeButtons)
            {
                textArray.Add(item);
                textArray.Add(new UICharacter(' '));
            }

            return textArray;
        }

        public void ChangeSpeed(int step)
        {
            UnfocusButton(actualSpeed);
            actualSpeed = Math.Clamp(actualSpeed+step,0,timeButtons.Count-1);
            FocusButton(actualSpeed);
            
            OnUpdate();
            OnSpeedChange(actualSpeed);
        }

        private void UnfocusButton(int buttonIndex)
        {
            timeButtons[buttonIndex].ChangeTextColor(ConsoleColor.White);
        }

        private void FocusButton(int buttonIndex)
        {
            var color = ConsoleColor.White;

            switch (buttonIndex)
            {
                case 0:
                    color = ConsoleColor.Red;
                    break;
                case 1:
                    color = ConsoleColor.Yellow;
                    break;
                case 2:
                    color = ConsoleColor.DarkGreen;
                    break;
                case 3:
                    color = ConsoleColor.Green;
                    break;
                default:
                    break;
            }

            timeButtons[buttonIndex].ChangeTextColor(color);
        }
    }
}

﻿using EnvSimulation.Console.UI.Text;
using EnvSimulation.Core.Communication;
using System;
using System.Collections.Generic;

namespace EnvSimulation.Console.UI.FrameElements
{
    class StatisticsView : FocusableUIFrameElement
    {
        private List<UIString> content;
        private FieldState currentField;
        
        public StatisticsView(string id): base(id)
        {
            content = new List<UIString>();
            currentField = null;
        }

        public override void ClickCursor()
        {
            throw new NotImplementedException();
        }

        public override UITextArray Draw()
        {
            var textArray = new UITextArray();

            foreach (var uiString in content)
            {
                textArray.AddLine(uiString);
            }

            return textArray;
        }

        public override void Focus()
        {
            throw new NotImplementedException();
        }

        public override void MoveCursor(int moveX, int moveY)
        {
            throw new NotImplementedException();
        }

        public override void UnFocus()
        {
            throw new NotImplementedException();
        }

        public void ChangeContent(FieldState field)
        {
            if (currentField == field)
                return;
            
            content.Clear();

            if (!(field.Creature is null))
            {
                var speciesName = field.Creature.SpeciesName;
                var hunger = field.Creature.Hunger;
                var hp = field.Creature.Health;

                content.Add(new UIString("Creature"));
                content.Add(new UIString("Species name:"));
                content.Add(new UIString(speciesName));
                content.Add(new UIString($"Hunger: {hunger}"));
                content.Add(new UIString($"Health: {hp}"));
            }
            else
            {
                if (field.Elements.Length == 0)
                {
                    content.Add(new UIString("Terrain"));

                    var typeString = field.TerrainType == 0 ? "Ground" : "Mountain";

                    content.Add(new UIString(typeString));
                }
                else
                {
                    var element = field.Elements[0];
                    var elementName = element.Name;

                    content.Add(new UIString("Element"));
                    content.Add(new UIString(elementName));
                }
            }

            currentField = field;
            OnUpdate();
        }
    }
}

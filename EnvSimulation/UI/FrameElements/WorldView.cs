﻿using EnvSimulation.Console.UI.Text;
using EnvSimulation.Core.Communication;
using EnvSimulation.Core.Envirnoment;
using System;
using System.Collections.Generic;
using System.Text;

namespace EnvSimulation.Console.UI.FrameElements
{
    class WorldView : FocusableUIFrameElement
    {
        private List<string> worldLines;
        private Dictionary<int, char> terrainSpriteDict;
        private Dictionary<string, char> interactorSpriteDict;
        public Position cursorPosition;
        private WorldState currentState;

        private bool isDrawing = false;

        public WorldView(string id, Dictionary<int, char> terrainSpriteDict, Dictionary<string, char> interactorSpriteDict) : base(id)
        {
            cursorPosition = new Position { X = 0, Y = 0 };
            worldLines = new List<string>();
            this.terrainSpriteDict = terrainSpriteDict;
            this.interactorSpriteDict = interactorSpriteDict;
        }

        public override UITextArray Draw()
        {
            isDrawing = true;
            if (worldLines.Count == 0)
                return new UITextArray();

            var textArray = new UITextArray();
            int i = 0;
            foreach (var line in worldLines)
            {
                var uiLine = new UIString(line);

                uiLine = ColorLine(uiLine, i++);

                textArray.AddLine(uiLine);
            }
            isDrawing = false;
            return textArray;
        }


        private UIString ColorLine(UIString line, int positionY)
        {
            for (int x = 0; x < line.GetLength(); x++)
            {
                var creature = currentState.Fields[x, positionY].Creature;

                if(positionY == cursorPosition.Y && x == cursorPosition.X)
                {
                    line[x].ChangeBackgroundColor(ConsoleColor.Blue);
                }

                if (creature is null)
                {
                    if(currentState.Fields[x,positionY].TerrainType == 1)
                        line[x].ChangeTextColor(ConsoleColor.Yellow);
                    continue;
                }

                if (creature.InCombat)
                    line[x].ChangeTextColor(ConsoleColor.Red);

                if (creature.Mating)
                    line[x].ChangeTextColor(ConsoleColor.Green);

                
            }

            return line;
        }

        public void UpdateWorldState(WorldState worldState)
        {
            var terrain = worldState.Fields;
            var width = terrain.GetLength(0);
            var height = terrain.GetLength(1);

            var stringTerrainBuilder = new StringBuilder();
            worldLines.Clear();

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    if(x == cursorPosition.X && y== cursorPosition.Y)
                    {
                        stringTerrainBuilder.Append('V');
                        continue;
                    }

                    var field = terrain[x, y];

                    var terrainSprite = GetTerrainFieldSprite(field);

                    stringTerrainBuilder.Append(terrainSprite);
                }

                worldLines.Add(stringTerrainBuilder.ToString());
                stringTerrainBuilder.Clear(); 
            }
            currentState = worldState;
            OnUpdate();
        }

        private char GetTerrainFieldSprite(FieldState field)
        {
            char sprite;
            if (field.Creature is null)
            {
                sprite = terrainSpriteDict[field.TerrainType];
                foreach (var interactor in field.Elements)
                {
                    sprite = interactorSpriteDict[interactor.Name];
                }
            }
            else
                sprite = (char)(field.Creature.SpeciesOrder + 64);

            return sprite;
        }

        public override void MoveCursor(int moveX, int moveY)
        {
            if (isDrawing)
                return;

            worldLines[cursorPosition.Y] = RefreshFiledInWorldLine(cursorPosition.X, cursorPosition.Y);

            var worldWidth = currentState.Fields.GetLength(0);
            var worldHeight = currentState.Fields.GetLength(1);

            cursorPosition.X = Math.Clamp(cursorPosition.X + moveX,0,worldWidth-1);
            cursorPosition.Y = Math.Clamp(cursorPosition.Y + moveY, 0, worldHeight-1);
            worldLines[cursorPosition.Y] = RefreshCursor();

            OnUpdate();
        }

        private string RefreshFiledInWorldLine(int positionX, int positionY)
        {
            var lineBuilder = new StringBuilder(worldLines[positionY]);

            var field = currentState.Fields[positionX, positionY];
            var newSprite = GetTerrainFieldSprite(field);

            lineBuilder[positionX] = newSprite;

            return lineBuilder.ToString();
        }

        private string RefreshCursor()
        {
            var lineBuilder = new StringBuilder(worldLines[cursorPosition.Y]);

            lineBuilder[cursorPosition.X] = 'V';

            return lineBuilder.ToString();
        }

        public override void Focus()
        {
        }

        public override void UnFocus()
        {
        }

        public override void ClickCursor()
        {
        }
    }
}

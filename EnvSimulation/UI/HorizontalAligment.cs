﻿namespace EnvSimulation.Console.UI
{
    public enum HorizontalAligment
    {
        Left,
        Right,
        Center
    }

    public enum VerticalAligment
    {
        Top,
        Center,
        Bottom
    }
}

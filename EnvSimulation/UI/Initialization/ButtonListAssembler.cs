﻿using System.Collections.Generic;

namespace EnvSimulation.Console.UI.Initialization
{
    class ButtonListAssembler : IFocusableElementAssembler
    {
        private List<UIButton> buttons;

        public ButtonListAssembler()
        {
            buttons = new List<UIButton>();
        }

        public ButtonListAssembler WithButton(UIButton button)
        {
            buttons.Add(button);

            return this;
        }

        public FocusableUIFrameElement Build()
        {
            var list = new UIButtonList();

            foreach (var button in buttons)
            {
                list.AddInteractiveElement(button);
            }

            return list;
        }
    }
}

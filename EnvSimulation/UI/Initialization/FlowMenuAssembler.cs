﻿

using System.Collections.Generic;

namespace EnvSimulation.Console.UI.Initialization
{
    class FlowMenuAssembler: IFocusableElementAssembler
    {
        private List<(FocusableUIFrameElement,string)> elementsToAdd;
        private IFocusableElementAssembler activeAssembler;
        private string activeAssemblerLabel;
        private string id;

        public FlowMenuAssembler()
        {
            elementsToAdd = new List<(FocusableUIFrameElement, string)>();
        }

        private void ChangeAssembler(IFocusableElementAssembler assembler, string label)
        {
            if (activeAssembler != null)
                elementsToAdd.Add((activeAssembler.Build(), activeAssemblerLabel));

            activeAssembler = assembler;
            activeAssemblerLabel = label;
        }

        public ButtonListAssembler WithButtonList(string label)
        {
            var assembler = new ButtonListAssembler();

            ChangeAssembler(assembler, label);

            return assembler;
        }

        public FlowMenuAssembler WithFlowMenu(string label)
        {
            var assembler = new FlowMenuAssembler();

            ChangeAssembler(assembler, label);

            return assembler;
        }

        public FlowMenuAssembler WithCustomElement(FocusableUIFrameElement element,string label)
        {
            elementsToAdd.Add((element,label));
            return this;
        }

        public FlowMenuAssembler WithId(string id)
        {
            this.id = id;
            return this;
        }

        public UiFlowMenu Build()
        {
            if (string.IsNullOrEmpty(id))
                id = "";

            var menu = new UiFlowMenu(id);

            foreach (var item in elementsToAdd)
            {
                var element = item.Item1;
                var label = item.Item2;

                menu.AddSubMenu(element, label);
            }

            return menu;
        }

        FocusableUIFrameElement IFocusableElementAssembler.Build()
        {
            return Build();
        }
    }
}

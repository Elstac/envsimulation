﻿namespace EnvSimulation.Console.UI.Initialization
{
    interface IFocusableElementAssembler
    {
        FocusableUIFrameElement Build();
    }
}

﻿using EnvSimulation.Console.UI.Text;
using System;
using System.Collections.Generic;

namespace EnvSimulation.Console.UI
{
    abstract class LinearUIElementList : FocusableUIFrameElement
    {
        protected List<FocusableUIFrameElement> interactiveElements;
        protected List<UIFrameElement> allElements;
        protected int activeIndex;

        public LinearUIElementList()
        {
            allElements = new List<UIFrameElement>();
            interactiveElements = new List<FocusableUIFrameElement>();
            activeIndex = 0;
        }

        public virtual void AddInteractiveElement(FocusableUIFrameElement element)
        {
            interactiveElements.Add(element);
            allElements.Add(element);

            element.Updated += () => OnUpdate();
        }

        public virtual void AddStaticElement(UIFrameElement element)
        {
            allElements.Add(element);
        }

        public override void ClickCursor()
        {
            interactiveElements[activeIndex].ClickCursor();
        }

        public override UITextArray Draw()
        {
            var array = new UITextArray();

            foreach (var element in allElements)
            {
                var elementArray = element.Draw();

                foreach (var uiText in elementArray.GetContent())
                {
                    array.AddLine(uiText);
                }
            }

            return array;
        }

        public override void Focus()
        {
            interactiveElements[activeIndex].Focus();
            OnUpdate();
        }

        public override void MoveCursor(int moveX, int moveY)
        {
            if (moveY != 0)
            {
                interactiveElements[activeIndex].UnFocus();
                activeIndex = Math.Clamp(activeIndex + moveY, 0, interactiveElements.Count - 1);
                interactiveElements[activeIndex].Focus();
                OnUpdate();

                return;
            }

            interactiveElements[activeIndex].MoveCursor(moveX, moveY);
        }

        public override void UnFocus()
        {
            interactiveElements[activeIndex].UnFocus();
            OnUpdate();
        }

        public override IEnumerable<UIElement> GetContent()
        {
            var content = new List<UIElement>();

            foreach (var element in interactiveElements)
            {
                content.AddRange(element.GetContent());
            }

            content.Add(this);

            return content;
        }
    }
}

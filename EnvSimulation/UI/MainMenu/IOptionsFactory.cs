﻿using System.Collections.Generic;

namespace EnvSimulation.Console.UI.MainMenu
{
    interface IOptionsFactory
    {
        List<(List<string>, string)> GetOptionsList();
    }
}

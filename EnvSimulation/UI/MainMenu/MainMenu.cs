﻿using System.Collections.Generic;
using EnvSimulation.Console.UI.Initialization;
using EnvSimulation.Console.UI.Text;

namespace EnvSimulation.Console.UI.MainMenu
{
    class MainMenu : FocusableUIFrameElement
    {
        private UiFlowMenu buttons;
        private UILabel menuLabel;

        public MainMenu(string id) : base(id)
        {
            var menuAssembler = new FlowMenuAssembler();

            buttons = menuAssembler
                .WithCustomElement(new WorldCreationMenu(),"Start")
                .WithCustomElement(new UIButton("exit_button", "Exit"), "Exit")
                .Build();

            menuLabel = new UILabel("Main menu", false);

            buttons.Updated += () => OnUpdate();
        }

        public override void ClickCursor()
        {
            buttons.ClickCursor();
        }

        public override UITextArray Draw()
        {
            var ret = menuLabel.Draw();

            var buttonsArray = buttons.Draw();
            

            foreach (var button in buttonsArray.GetContent())
                ret.AddLine(button);

            return ret;
        }

        public override void Focus()
        {
            buttons.Focus();
        }

        public override void MoveCursor(int moveX, int moveY)
        {
            buttons.MoveCursor(moveX, moveY);
        }

        public override void UnFocus()
        {
        }

        public override IEnumerable<UIElement> GetContent()
        {
            var content = new List<UIElement>();

            content.Add(this);
            content.AddRange(buttons.GetContent());

            return content;
        }
    }
}

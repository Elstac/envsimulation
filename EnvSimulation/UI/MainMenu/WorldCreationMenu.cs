﻿using EnvSimulation.Shared.Configuration;
using System.Collections.Generic;

namespace EnvSimulation.Console.UI.MainMenu
{
    class WorldCreationMenu : LinearUIElementList
    {
        private List<UIOptionPanel> optionPanels;

        public WorldCreationMenu()
        {
            optionPanels = new List<UIOptionPanel>();

            var terrainOptions = WorldOptionsFactory.GetTerrainOptions();

            foreach (var option in terrainOptions)
            {
                var optionPanel = new UIOptionPanel(option.Item1, option.Item2);
                optionPanels.Add(optionPanel);
            }

            AddStaticElement(new UILabel("Terrain",true));
            foreach (var panel in optionPanels)
            {
                AddInteractiveElement(panel);
            }


            var speciesOptions = WorldOptionsFactory.GetSpeciesOptions();

            AddStaticElement(new UILabel("Species",true));
            foreach (var option in speciesOptions)
            {
                var optionPanel = new UIOptionPanel(option.Item1, option.Item2);
                optionPanels.Add(optionPanel);
                AddInteractiveElement(optionPanel);
            }

            AddInteractiveElement(new UIButton("start_button","Start game"));
        }

        public WorldGenerationOptions GetOptions()
        {
            var optionsCollection = new List<Option>();

            foreach (var optionPanel in optionPanels)
            {
                var option = optionPanel.GetSelectedOption();
                optionsCollection.Add(option);
            }

            var retOptions = new WorldGenerationOptions()
            {
                WorldSize = optionsCollection.Find(opt => opt.Name == "worldsize").Value,
                TerrainType = optionsCollection.Find(opt => opt.Name == "terraintype").Value,
                Behaviour = optionsCollection.Find(opt => opt.Name == "behaviour").Value,
                CreaturePerSpecies = int.Parse(optionsCollection.Find(opt => opt.Name == "creaturesperspecies").Value),
                SpeciesNumber = int.Parse(optionsCollection.Find(opt => opt.Name == "speciesnumber").Value),
                FoodDensity = optionsCollection.Find(opt => opt.Name == "fooddensity").Value,
            };

            return retOptions;
        }
    }
}

﻿using System.Collections.Generic;

namespace EnvSimulation.Console.UI.MainMenu
{
    static class WorldOptionsFactory
    { 
        public static List<(List<string>, string)> GetTerrainOptions()
        {
            return new List<(List<string>, string)>()
            {
                (new List<string>{"small","medium","big"},"WorldSize"),
                (new List<string>{"square","rectangle"},"WorldShape"),
                (new List<string>{"plains","mountains","empty"},"TerrainType"),
                (new List<string>{"low","medium","high"},"FoodDensity"),
            };
        }

        public static List<(List<string>, string)> GetSpeciesOptions()
        {
            return new List<(List<string>, string)>()
            {
                (new List<string>{"passive","mixed","aggressive"},"Behaviour"),
                (new List<string>{"1","2","3","4","5","6"},"SpeciesNumber"),
                (new List<string>{"1","2","3","4","5","6"},"CreaturesPerSpecies"),
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace EnvSimulation.Console.UI.Management
{
    class UIManager
    {
        private List<UIScene> scenes;
        private UIScene activeScene;

        public UIManager()
        {
            scenes = new List<UIScene>();
        }

        public TElement FindUIElementById<TElement>(string id) where TElement : UIElement
        {
            return activeScene.FindUIElementById<TElement>(id);
        }

        public TElement FindUIElementOfType<TElement>() where TElement : UIElement
        {
            return activeScene.FindUIElementOfType<TElement>();
        }

        public void AddScene(UIScene scene)
        {
            scenes.Add(scene);
        }

        public void ChangeScene(int sceneIndex)
        {
            if(!(activeScene is null))
                activeScene.ClearScene();

            activeScene = scenes[sceneIndex];

            activeScene.ActivateScene();
        }

        public void BindActionToButton(string buttonId, Action<object,EventArgs> action)
        {
            FindUIElementById<UIButton>(buttonId).OnClick += new UIButton.ButtonClickHanlder(action);
        }
    }
}

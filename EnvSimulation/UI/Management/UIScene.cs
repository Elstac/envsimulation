﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;

namespace EnvSimulation.Console.UI.Management
{
    class UIScene
    {
        private List<UIElement> elements;
        private List<UIFrame> rootFrames;
        private Queue<Action> drawQueue;

        private Timer drawingTimer;
        private object drawing = new object();

        public UIScene()
        {
            elements = new List<UIElement>();
            rootFrames = new List<UIFrame>();
            drawQueue = new Queue<Action>();

            drawingTimer = new Timer(10);

            drawingTimer.Elapsed += (sender, args) =>
            {
                lock (drawing)
                {
                    if (drawQueue.Count <= 0)
                        return;

                    var drawAction = drawQueue.Dequeue();
                    drawAction();
                }
            };
        }

        public void AddUIFrame(UIFrame frame)
        {
            elements.AddRange(frame.GetContent());
            elements.Add(frame);
            rootFrames.Add(frame);

            frame.OnContentUpdate += (action) => drawQueue.Enqueue(action);
        }

        public void ActivateScene()
        {
            foreach (var frame in rootFrames)
                frame.Draw();

            drawingTimer.Start();
        }

        public void ClearScene()
        {
            drawingTimer.Stop();

            lock(drawing)
            {
                drawQueue.Clear();
            }
        }

        public TElement FindUIElementById<TElement>(string id) where TElement : UIElement
        {
            var element = elements.SingleOrDefault(e => e.Id == id);

            if (element is null)
                throw new System.Exception($"No element with id {id} found");

            if (element is TElement)
                return element as TElement;

            throw new System.Exception($"No element of type {typeof(TElement).Name} and id {id}");
        }

        public TElement FindUIElementOfType<TElement>() where TElement : UIElement
        {
            var element = elements.SingleOrDefault(e => e is TElement);

            if(element is null)
                throw new System.Exception($"No element of type {typeof(TElement).Name}");

            return element as TElement;
        }
    }
}

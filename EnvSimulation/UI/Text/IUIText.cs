﻿using System;
using System.Collections.Generic;

namespace EnvSimulation.Console.UI.Text
{
    interface IUIText
    {
        int GetLength();
        IEnumerable<UICharacter> GetContent();
        void ChangeTextColor(ConsoleColor color);
        void ChangeBackgroundColor(ConsoleColor color);
        IUIText Add(IUIText text);
    }
}

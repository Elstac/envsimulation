﻿using System;
using System.Collections.Generic;

namespace EnvSimulation.Console.UI.Text
{
    class UICharacter: IUIText
    {
        public char Character { get; set; }
        public ConsoleColor TextColor { get; set; }
        public ConsoleColor Background { get; set; }

        public UICharacter()
        {
            Character = ' ';
            TextColor = ConsoleColor.White;
            Background = ConsoleColor.Black;
        }

        public UICharacter(char character)
        {
            Character = character;
            TextColor = ConsoleColor.White;
            Background = ConsoleColor.Black;
        }

        public UICharacter(char character,ConsoleColor textColor, ConsoleColor backgroundColor)
        {
            Character = character;
            TextColor = textColor;
            Background = backgroundColor;
        }

        public void Draw()
        {
            System.Console.ForegroundColor = TextColor;
            System.Console.BackgroundColor = Background;
            System.Console.Write(Character);
            System.Console.ForegroundColor = ConsoleColor.Gray;
            System.Console.BackgroundColor = ConsoleColor.Black;
        }

        public int GetLength()
        {
            return 1;
        }

        public IEnumerable<UICharacter> GetContent()
        {
            return new UICharacter[] { this };
        }

        public void ChangeTextColor(ConsoleColor color)
        {
            TextColor = color;
        }

        public void ChangeBackgroundColor(ConsoleColor color)
        {
            Background = color;
        }

        public IUIText Add(IUIText text)
        {
            var uiString = new UIString();
            uiString.Add(this);
            uiString.Add(text);

            return uiString;
        }
    }
}

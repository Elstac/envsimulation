﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EnvSimulation.Console.UI.Text
{
    class UIString:IUIText
    {
        private List<IUIText> components;

        private ConsoleColor textColor;
        private ConsoleColor background;
        private List<UICharacter> characrerList;
        private bool changed;

        public ConsoleColor TextColor { get=>textColor;}
        public ConsoleColor Background { get => background;}

        public UIString()
        {
            components = new List<IUIText>();
            textColor = ConsoleColor.White;
            background = ConsoleColor.Black;
            changed = false;
        }

        public UIString(string text)
        {
            components = new List<IUIText>();
            foreach (var charcter in text)
                Add(new UICharacter(charcter));

            textColor = ConsoleColor.White;
            background = ConsoleColor.Black;
        }

        public int GetLength()
        {
            var totalLen = 0;
            foreach (var component in components)
                totalLen += component.GetLength();

            return totalLen;
        }

        public IEnumerable<UICharacter> GetContent()
        {
            var ret = new List<UICharacter>();
            foreach (var component in components)
                ret.AddRange(component.GetContent());

            return ret;
        }

        public void ChangeTextColor(ConsoleColor color)
        {
            textColor = color;
            foreach (var comp in components)
                comp.ChangeTextColor(color);
        }

        public void ChangeBackgroundColor(ConsoleColor color)
        {
            background = color; 
            foreach (var comp in components)
                comp.ChangeBackgroundColor(color);
        }

        public IUIText Add(IUIText text)
        {
            components.Add(text);
            changed = true;
            return this;
        }


        public static UIString operator+(UIString @string, IUIText uiText)
        {
            @string.components.Add(uiText);
            return @string;
        }

        public UICharacter this[int position]
        {
            get
            {
                if (characrerList is null || changed)
                {
                    characrerList = components.Where(c => c is UICharacter).Select(c => c as UICharacter).ToList();
                    changed = false;
                }
                return characrerList[position];
            }

            set
            {
                if (characrerList is null || changed)
                {
                    characrerList = components.Where(c => c is UICharacter).Select(c => c as UICharacter).ToList();
                    changed = false;
                }
                characrerList[position] = value;
                changed = true;
            }
        }
    }
}

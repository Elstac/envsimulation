﻿using System.Collections.Generic;
using System.Linq;

namespace EnvSimulation.Console.UI.Text
{
    class UITextArray
    {
        List<IUIText> content;

        public UITextArray()
        {
            content = new List<IUIText>();
        }

        public void AddLine(IUIText line)
        {
            var s = new UIString();
            s.Add(line);
            content.Add(s);
        }

        public void Add(IUIText text)
        {
            if (content.Count == 0)
                AddLine(text);
            else
                content[content.Count - 1].Add(text);
        }

        public int GetHeight()
        {
            return content.Count;
        }

        public int GetWidth()
        {
            if (content.Count == 0)
                return 0;

            return content.Max(text=>text.GetLength());
        }

        public List<IUIText> GetContent()
        {
            return content;
        }
    }
}

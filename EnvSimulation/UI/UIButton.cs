﻿using EnvSimulation.Console.UI.MainMenu;
using EnvSimulation.Console.UI.Text;
using System;
using System.Collections.Generic;

namespace EnvSimulation.Console.UI
{
    class UIButton:FocusableUIFrameElement
    {
        private UIString text;
        public delegate void ButtonClickHanlder(object sender, EventArgs args);
        public event ButtonClickHanlder OnClick;

        public UIButton(string id, string content): base(id)
        {
            text = new UIString(content);
        }

        public UIButton(string content) : base("")
        {
            text = new UIString(content);
        }

        public override UITextArray Draw()
        {
            var array = new UITextArray();

            array.Add(text);

            return array;
        }

        public override void Focus()
        {
            text.ChangeBackgroundColor(ConsoleColor.White);
            text.ChangeTextColor(ConsoleColor.Black);
        }

        public override void UnFocus()
        {
            text.ChangeBackgroundColor(ConsoleColor.Black);
            text.ChangeTextColor(ConsoleColor.White);
        }


        public override IEnumerable<UIElement> GetContent()
        {
            return new UIElement[] { this };
        }

        public override void MoveCursor(int moveX, int moveY)
        {
        }

        public override void ClickCursor()
        {
            OnClick(this, EventArgs.Empty);
        }
    }
}

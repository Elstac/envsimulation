﻿using System.Collections.Generic;

namespace EnvSimulation.Console.UI
{
    abstract class UIElement
    {
        protected UIElement(string id)
        {
            Id = id;
        }

        public string Id {get;}
        public abstract IEnumerable<UIElement> GetContent();
    }
}

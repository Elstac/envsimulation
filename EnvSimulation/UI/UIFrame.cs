﻿using EnvSimulation.Console.UI.Text;
using System;
using System.Collections.Generic;
using System.Text;

namespace EnvSimulation.Console.UI
{
    class UIFrame: UIElement
    {
        private int posX;
        private int posY;

        private int width;
        private int height;

        private UIFrameElement content;

        public delegate void UpdateContentEventHandler(Action action);
        public event UpdateContentEventHandler OnContentUpdate;

        public UIFrame(string id, int posX, int posY, int width, int height) : base(id)
        {
            this.posX = posX;
            this.posY = posY;
            this.width = width;
            this.height = height;
        }

        public void Draw()
        {
            var sb = new StringBuilder();
            sb.Append('=', width);
            var horizontalBorder = sb.ToString();
            sb.Clear();
            sb.Append('|');
            sb.Append(' ', width - 2);
            sb.Append('|');
            var middlePart = sb.ToString();

            System.Console.SetCursorPosition(posX, posY);
            System.Console.Write(horizontalBorder);
            System.Console.SetCursorPosition(posX, posY+1);
            for (int i = 0; i < height - 2; i++)
            {
                System.Console.Write(middlePart);
                System.Console.SetCursorPosition(posX, posY + 2 +i);
            }

            System.Console.Write(horizontalBorder);

            if(content!=null)
                DrawContent();
        }

        private void DrawContent()
        {
            var arrayToDraw = content.Draw();

            var contentWidth = arrayToDraw.GetWidth();
            contentWidth = contentWidth > width - 2 ? width - 2 : contentWidth;

            var contentHeight = arrayToDraw.GetHeight();
            contentHeight = contentHeight > height - 1 ? height - 1 : contentHeight;

            var contentPosX = CalculateContentPositionX(contentWidth);
            var contentPosY = CalculateContentPositionY(contentHeight);

            System.Console.SetCursorPosition(contentPosX, contentPosY);

            var textCollection = arrayToDraw.GetContent();
            for (int y = 0; y < contentHeight; y++)
            {
                foreach (var character in textCollection[y].GetContent())
                {
                    character.Draw();
                }
                System.Console.SetCursorPosition(contentPosX, contentPosY + 1 + y);
            }

            ClearContentBackground(arrayToDraw);
        }

        private void ClearContentBackground(UITextArray arrayToDraw)
        {
            System.Console.SetCursorPosition(posX + 1, posY + 1);
            for (int y = posY + 1; y < posY + height -1; y++)
            {
                System.Console.SetCursorPosition(posX + 1, y);
                for (int x = posX+1; x < posX + width - 1; x++)
                {
                    if (!PixelInsideContent(arrayToDraw, x, y))
                        System.Console.Write(' ');
                    else
                        System.Console.SetCursorPosition(x + 1, y);
                }
            }
        }

        private bool PixelInsideContent(UITextArray contentArray, int x, int y)
        {
            var contentPosX = CalculateContentPositionX(contentArray.GetWidth());
            var contentPosY = CalculateContentPositionY(contentArray.GetHeight());

            if (contentPosX > x || x >= contentPosX + contentArray.GetWidth() ||
                contentPosY > y || y >= contentPosY + contentArray.GetHeight())
                return false;

            var realativeContentPosX = x - contentPosX;

            if (contentArray.GetContent()[y - contentPosY].GetLength() > realativeContentPosX &&
                realativeContentPosX >= 0)
                return true;

            return false;
        }

        private int CalculateContentPositionY(int contentHeight)
        {
            switch (content.VerticalAligment)
            {
                case VerticalAligment.Top:
                    return posY + 1;
                case VerticalAligment.Center:
                    return posY + 1 + (height - contentHeight) / 2 - 1;
                case VerticalAligment.Bottom:
                    return posY + height - contentHeight - 1;
                default:
                    throw new System.Exception("Cannot determine UIFrame element vertical aligment");
            }
        }

        private int CalculateContentPositionX(int contentWidth)
        {

            switch (content.HorizontalAligment)
            {
                case HorizontalAligment.Left:
                    return posX + 1;
                case HorizontalAligment.Right:
                    return posX + width - contentWidth - 1;
                case HorizontalAligment.Center:
                    return posX + 1 + (width - contentWidth) / 2 - 1;
                default:
                    throw new System.Exception("Cannot determine UIFrame element horizontal aligment");
            }
        }

        public void SetContent(UIFrameElement content)
        {
            this.content = content;
            content.Updated += () => OnContentUpdate(() => DrawContent());
        }

        public override IEnumerable<UIElement> GetContent()
        {
            return content.GetContent();
        }
    }
}

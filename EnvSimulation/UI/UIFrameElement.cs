﻿using System.Collections.Generic;
using EnvSimulation.Console.UI.Text;

namespace EnvSimulation.Console.UI
{
    
    abstract class UIFrameElement:UIElement
    {
        public HorizontalAligment HorizontalAligment { get; set; }
        public VerticalAligment VerticalAligment { get; set; }
        public delegate void OnFrameElementUpdateHandler();
        public event OnFrameElementUpdateHandler Updated;

        public UIFrameElement() : base("")
        {
            HorizontalAligment = HorizontalAligment.Left;
            VerticalAligment = VerticalAligment.Top;
        }

        public UIFrameElement(string id) : base(id)
        {
            HorizontalAligment = HorizontalAligment.Left;
            VerticalAligment = VerticalAligment.Top;
        }
        protected virtual void OnUpdate()
        {
            Updated();
        }

        public abstract UITextArray Draw();

        public override IEnumerable<UIElement> GetContent()
        {
            return new List<UIElement> { this };
        }
    }
}

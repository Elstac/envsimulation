﻿using EnvSimulation.Console.UI.Text;
using System;
using System.Collections.Generic;
using System.IO;

namespace EnvSimulation.Console.UI
{
    class UIImage : UIFrameElement
    {
        private List<UIString> graphic;

        public UIImage(string filePath)
        {
            graphic = new List<UIString>();

            using(var sr = new StreamReader(filePath))
            {
                while (!sr.EndOfStream)
                {
                    var line = sr.ReadLine();

                    graphic.Add(new UIString(line));
                }
            }
        }
        public override UITextArray Draw()
        {
            var ret = new UITextArray();

            foreach (var line in graphic)
            {
                ret.AddLine(line);
            }

            return ret;
        }
    }
}

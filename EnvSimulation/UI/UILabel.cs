﻿using EnvSimulation.Console.UI.Text;
using System.Text;

namespace EnvSimulation.Console.UI
{
    class UILabel : UIFrameElement
    {
        private string text;
        private bool hasBorder;

        public UILabel(string text, bool hasBorder)
        {
            this.text = text;
            this.hasBorder = hasBorder;
        }

        public override UITextArray Draw()
        {
            var array = new UITextArray();

            if (hasBorder)
            {
                var borderBuilder = new StringBuilder();
                borderBuilder.Append('=', text.Length + 2);

                array.AddLine(new UIString(borderBuilder.ToString()));
                array.AddLine(new UIString("|"+text+"|"));
                array.AddLine(new UIString(borderBuilder.ToString()));
            }
            else
                array.Add(new UIString(text));

            return array;
        }
    }
}

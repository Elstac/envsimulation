﻿using EnvSimulation.Shared.Configuration;
using EnvSimulation.Console.UI.Text;
using System;
using System.Collections.Generic;

namespace EnvSimulation.Console.UI
{
    class UIOptionPanel : FocusableUIFrameElement
    {
        private List<string> optionValues;
        private string label;
        private int actualIndex;
        private bool focused;

        public UIOptionPanel(List<string> optionValues, string label)
        {
            this.optionValues = optionValues;
            this.label = label;

            focused = false;
            actualIndex = 0;
        }

        public Option GetSelectedOption()
        {
            return new Option { Value = optionValues[actualIndex], Name = label.ToLower() };
        }


        public override void ClickCursor()
        {
        }

        public override UITextArray Draw()
        {
            var ret = new UITextArray();
            ret.Add(new UIString(label+" "));


            if (actualIndex > 0)
                ret.Add(new UICharacter('<'));
            else
                ret.Add(new UICharacter(' '));

            var optionString = new UIString(optionValues[actualIndex]);

            if (focused)
            {
                optionString.ChangeBackgroundColor(ConsoleColor.White);
                optionString.ChangeTextColor(ConsoleColor.Black);
            }

            ret.Add(optionString);

            if (actualIndex < optionValues.Count-1)
                ret.Add(new UICharacter('>'));

            return ret;
        }

        public override void Focus()
        {
            focused = true;
            OnUpdate();
        }

        public override void MoveCursor(int moveX, int moveY)
        {
            actualIndex = Math.Clamp(actualIndex+moveX,0,optionValues.Count-1);
            OnUpdate();
        }

        public override void UnFocus()
        {
            focused = false;
            OnUpdate();
        }
    }
}

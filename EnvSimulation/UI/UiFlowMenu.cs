﻿using EnvSimulation.Console.UI.Text;
using System.Collections.Generic;

namespace EnvSimulation.Console.UI
{
    class UiFlowMenu : FocusableUIFrameElement
    {
        List<FocusableUIFrameElement> submenus;
        FocusableUIFrameElement activeMenu;
        UIButtonList navigationMenu;

        public UiFlowMenu(string id):base(id)
        {
            submenus = new List<FocusableUIFrameElement>();
            navigationMenu = new UIButtonList();
            navigationMenu.Updated += () => OnUpdate();

            activeMenu = navigationMenu;
        }

        public override void ClickCursor()
        {
            activeMenu.ClickCursor();
        }

        public override UITextArray Draw()
        {
            return activeMenu.Draw();
        }

        public override void Focus()
        {
            activeMenu.Focus();
        }

        public override void MoveCursor(int moveX, int moveY)
        {
            activeMenu.MoveCursor(moveX, moveY);
        }

        public override void UnFocus()
        {
            activeMenu.UnFocus();
            activeMenu = navigationMenu;
        }

        public override IEnumerable<UIElement> GetContent()
        {
            var elementCollection = new List<UIElement>();

            foreach (var submenu in submenus)
            {
                elementCollection.AddRange(submenu.GetContent());
            }

            elementCollection.AddRange(navigationMenu.GetContent());
            elementCollection.Add(this);

            return elementCollection;
        }

        public void AddSubMenu(FocusableUIFrameElement buttonList, string label)
        {
            submenus.Add(buttonList);
            buttonList.Updated += () => OnUpdate();

            var navButton = new UIButton(label);

            navButton.OnClick += (sender,args) => {
                activeMenu.UnFocus();
                activeMenu = buttonList;
                activeMenu.Focus();
                };

            navigationMenu.AddInteractiveElement(navButton);
        }
    }
}
